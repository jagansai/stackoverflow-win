﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using StackOverflow.Common;
using StackOverflow.Data;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Linq;

// The Group Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234229

namespace StackOverflow
{
    /// <summary>
    /// A page that displays an overview of a single group, including a preview of the items
    /// within the group.
    /// </summary>

    /*
     *  Bugs
     *  1) Refreshing the data is not single step.
     *  2) Loading update is making the UI without any data.
     *  3) Code needs refactoring.
     * 
     */

    public sealed partial class GroupDetailPage : Page
    {
        private NavigationHelper navigationHelper;
        DispatcherTimer _timer = new DispatcherTimer();
        private static bool _backButtonPressed = false;
        bool _timerStopped = false;
        private GroupedItemsPage root = GroupedItemsPage.current;
        public static GroupDetailPage current;

        private int _questionId;
        private ObservableCollection<Question> _questions = null;
        private ObservableCollection<Comment> _comments = null;
        private ObservableCollection<Answer> _answers = null;
        
        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public void BackButtonPressed(bool pressed)
        {
            _backButtonPressed = pressed;
        }

        public GroupDetailPage()
        {
            this.InitializeComponent();

            _questions = new ObservableCollection<Question>();
            _comments = new ObservableCollection<Comment>();
            _answers = new ObservableCollection<Answer>();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            current = this;
        }

        public GroupedItemsPage Root
        {
            get
            {
                return root;
            }
        }


        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            _questionId = (int)e.NavigationParameter;
           // var question_id = 28787912;
//            var question_id = 29023382;

            var detailInfo = await StackOverflowSource.Instance.GetQuestionDetailsAsync(_questionId, GroupedItemsPage.current.UserDetails.AccessToken);
            SetSources(detailInfo);
            InitTimerStart(20);
            _timer.Tick += (s, ev) => CheckForUpdates(s, ev, _questionId);
        }


        private void SetSources(QuestionInfo detailInfo)
        {
                if (_questions.Count == 0)
                {
                    _questions.Add(detailInfo.items[0]);
                }
                else
                {
                    _questions[0] = detailInfo.items[0];
                }
                _comments.AddRange(detailInfo.items[0].comments, clearCollection:true);
                _answers.AddRange(detailInfo.items[0].answers, clearCollection:true);

                this.QuestionsSource.Source = _questions;
                this.QuestionCommentsSource.Source = _comments;
                this.AnswersSource.Source = _answers;
 
                this.itemGridView.Visibility = Visibility.Visible;
                this.UpdateLayout();
        }

        private async void CheckForUpdates(object sender, object e, int question_id)
        {
            try
            {
                if (!_timerStopped) 
                {
                    bool newUpdate = await StackOverflowSource.Instance.CheckForUpdate(question_id, _questions[0].last_activity_date, root.UserDetails.AccessToken);
                    if (newUpdate)
                    {
                        this.updateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        InitTimerStop();
                    }
                }
            }
            catch (InvalidOperationException)
            {
                // Understand how to log exceptions.
            }
        }

        private async void UpdateUI(Object sender, object e, int question_id)
        {
           var info = await StackOverflowSource.Instance.LoadQuestionAsync(question_id, root.UserDetails.AccessToken);
           SetSources(info);
        }
   
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var answerId = ((Answer)e.ClickedItem).answer_id;
            StackOverflowSource.Instance.SaveDetailInfo(_questions[0]);
            this.Frame.Navigate(typeof(ItemDetailPage), answerId);
        }

        private async void UpVoteQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
                {
                    GroupedItemsPage.Login();
                }
                else
                {
                    if (!_questions[0].upvoted)
                    {
                        var info = await StackOverflowSource.SetUpVote<QuestionInfo>(_questionId, root.UserDetails.AccessToken);
                        var sp = VisualTreeHelper.GetParent(sender as Button) as StackPanel;
                        var upVoteButton = sp.Children[0] as AppBarButton;
                        upVoteButton.IsEnabled = false;
                        _questions[0].score = info.items[0].score;
                        _questions[0].upvoted = info.items[0].upvoted;
                        this.UpdateLayout();
                    }
                }
            }
            catch(Exception)
            {
                // Eat the exceptions in most places.
            }
        }

        private async void DownVoteQuestionButton_Click(object sender, EventArgs e)
        {
            try
            {



                if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
                {
                    GroupedItemsPage.Login();
                }
                else
                {
                    if (!_questions[0].downvoted)
                    {
                        var info = await StackOverflowSource.SetDownVote<QuestionInfo>(_questionId, root.UserDetails.AccessToken);
                        var sp = VisualTreeHelper.GetParent(sender as Button) as StackPanel;
                        var downVoteButton = sp.Children[2] as AppBarButton;
                        downVoteButton.IsEnabled = false;
                        _questions[0].score = info.items[0].score;
                        _questions[0].downvoted = info.items[0].downvoted;
                        this.UpdateLayout();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            InitTimerStop();
            root.BackButtonPressed(true);
        }

        private void InitTimerStop()
        {
            _timerStopped = true;
            _timer.Stop();
        }

        private void InitTimerStart(int seconds)
        {
            _timer.Interval = new TimeSpan(0, 0, seconds);
            _timer.Start();
            _timerStopped = false;
        }

        private void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            SetSources(StackOverflowSource.Instance.DetailInfo);
            this.updateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            InitTimerStart(10); 
        }

        private void AddAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility.Visible);
                var scrollViewer = Utils.FindVisualChild<ScrollViewer>(this.itemGridView);
                if (scrollViewer != null)
                {
                    scrollViewer.ChangeView(scrollViewer.ScrollableWidth, null, null);
                }
            }
        }


        private void FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility state)
        {
            this.answerBox.Visibility = state;
            //this.submitButtonBorder.Visibility = state;
            answerButtons.Visibility = state;
        }

        private async void submitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string outText;
                this.answerBox.Document.GetText(
                    Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var answer = await QueryStackOverflow.AddAnswerAsync(this.root.UserDetails.AccessToken, _questionId, outText);
                if (answer != null)
                {
                    FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed);
                    _answers.Add(answer.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].answers.Add(answer.items[0]);
                    _questions[0].answer_count = _answers.Count;
                    this.UpdateLayout();
                    var scrollViewer = Utils.FindVisualChild<ScrollViewer>(this.itemGridView);
                    if (scrollViewer != null)
                    {
                        scrollViewer.ChangeView(scrollViewer.ScrollableWidth, null, null);
                    }
                }
            }
            catch(Exception)
            {
            }
        }


        private void answerBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            string text;
            this.answerBox.Document.GetText(
                 Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out text);

            if (text.Length > 15)
            {
                answerSubmitButton.IsEnabled = true;
            }
            else
            {
                answerSubmitButton.IsEnabled = false;
            }
        }


        private void FlipCommentBox(Windows.UI.Xaml.Visibility state)
        {
            var commentBox = Utils.FindVisualChild<CommentBoxControl>(this.commentBoxControl);
            commentBox.Visibility = state;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            FlipAnswerBoxVisibility(Visibility.Collapsed);
        }

        private void addCommentButton_Click(object sender, EventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                FlipCommentBox(Windows.UI.Xaml.Visibility.Visible);
                this.UpdateLayout();
                var verticalOffset = this.mainScrollViewer.VerticalOffset + 200;
                this.mainScrollViewer.ChangeView(null, Math.Min(verticalOffset, this.mainScrollViewer.ScrollableHeight), null);
            }
        }

        private void commentCancelButton_Click(object sender, EventArgs e)
        {
            FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
        }

        private async void commentsubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                var commentSubmitButton = this.commentBoxControl.GetFirstDescendantOfType<Button>();
                commentSubmitButton.IsEnabled = false;
                var commentEditBox = this.commentBoxControl.GetFirstDescendantOfType<RichEditBox>();
                
                string outText;
                commentEditBox.Document.GetText(Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var comment = await StackOverflowSource.AddCommentAsync(this.root.UserDetails.AccessToken, _questionId, outText);
                if (comment != null)
                {
                    FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
                    _comments.Add(comment.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].comments.Add(comment.items[0]);
                    _questions[0].comment_count = _comments.Count;
                    this.UpdateLayout();
                    this.mainScrollViewer.ChangeView(null, this.mainScrollViewer.ScrollableHeight, null);
                } // else will be an exception handling case. We'll handle exceptions later. By putting some info on the AppBar.
            }
            catch (Exception)
            {
            }
        }

        private void TagButton_Click(object sender, EventArgs e)
        {
            // Here... call the root page.
            this.Frame.Navigate(typeof(GroupedItemsPage), (sender as Button).Content as string);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateUI(null, null, _questionId); // Just call the UpdateUI.
        }

        private void questionProfileImageClick(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfilePage), this._questions[0].owner.user_id.ToString());
        }


    }
}