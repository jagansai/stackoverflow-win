﻿using System;
// Move most of the Hierarchy code to common utilities.

using System.Collections.ObjectModel;
using System.Linq;
using StackOverflow.Common;
using StackOverflow.Data;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;




// The Item Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234232

namespace StackOverflow
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class ItemDetailPage : Page
    {
        private NavigationHelper navigationHelper;
        private GroupDetailPage root = GroupDetailPage.current;

        private ObservableCollection<Answer> _answers = new ObservableCollection<Answer>();
        private ObservableCollection<Comment> _comments = new ObservableCollection<Comment>();

        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private bool _timerStopped = false;
        private int _answerId;
        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ItemDetailPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
        }

        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            _answerId = (int)e.NavigationParameter;
            _answers.AddRange(StackOverflowSource.Instance.DetailInfo.items[0].answers.Where(a => a.answer_id == _answerId));
            this.AnswerSource.Source = _answers;
            this.QuestionsSource.Source = new ObservableCollection<Question>(StackOverflowSource.Instance.DetailInfo.items);
            _comments.AddRange(_answers[0].comments.EmptyIfNull());
            this.AnswerComments.Source = _comments;
            this.itemGridView.Visibility = Visibility.Visible;
            _timer.Tick += (s, ev) => CheckForUpdates(s, ev, _answerId);
            InitTimerStart(20);
        }

        private async void CheckForUpdates(object sender, object e, int answerId)
        {
            try
            {
                if (!_timerStopped) 
                {
                    var answers = this.AnswerSource.Source as ObservableCollection<Answer>;
                    bool newUpdate = await StackOverflowSource.Instance.CheckForAnswerUpdate(answerId, answers[0].last_activity_date, root.Root.UserDetails.AccessToken);

                    if (newUpdate)
                    {
                        this.updateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //var button = this.AppBarControl.GetDescendantsOfType<Button>().Where(x => x.Name == "updateReadyButton").First();
                        //button.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        InitTimerStop();
                    }
                }
            }
            catch (InvalidOperationException)
            {
                // Understand how to log exceptions.
            }
        }

        private void InitTimerStop()
        {
            _timerStopped = true;
            _timer.Stop();
        }

        private void InitTimerStart(int seconds)
        {
            _timer.Interval = new TimeSpan(0, 0, seconds);
            _timer.Start();
            _timerStopped = false;
        }

        private void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            SetSources();
            this.updateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            InitTimerStart(10);
        }

        public async void UpVoteAnswerButton_Click(object sender, EventArgs args)
        {
            try
            {
                if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
                {
                    GroupedItemsPage.Login();
                }
                else
                {
                    if (!_answers[0].upvoted)
                    {
                        var info = await StackOverflowSource.SetUpVote<AnswerInfo>(_answerId, root.Root.UserDetails.AccessToken);
                        var sp = VisualTreeHelper.GetParent(sender as Button) as StackPanel;
                        var upVoteButton = sp.Children[0] as AppBarButton;
                        upVoteButton.IsEnabled = false;
                        _answers[0].score = info.items[0].score;
                        _answers[0].upvoted = info.items[0].upvoted;
                        this.UpdateLayout();
                    }
                }
            }
            catch (Exception)
            {                
            }
        }

        public async void DownVoteAnswerButton_Click(object sender, EventArgs args)
        {
            try
            {
                if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
                {
                    GroupedItemsPage.Login();
                }
                else
                {
                    if (!_answers[0].downvoted)
                    {
                        var info = await StackOverflowSource.SetDownVote<AnswerInfo>(_answerId, root.Root.UserDetails.AccessToken);
                        var sp = VisualTreeHelper.GetParent(sender as Button) as StackPanel;
                        var downVoteButton = sp.Children[2] as AppBarButton;
                        downVoteButton.IsEnabled = false;
                        _answers[0].score = info.items[0].score;
                        _answers[0].downvoted = info.items[0].downvoted;
                        this.UpdateLayout();
                    }
                }
            }
            catch (Exception)
            {             
            }
        }


        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            InitTimerStop();
            root.BackButtonPressed(true);
        }

        private async void commentsubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                var commentSubmitButton = this.commentBoxControl.GetFirstDescendantOfType<Button>();
                commentSubmitButton.IsEnabled = false;
                var commentEditBox = this.commentBoxControl.GetFirstDescendantOfType<RichEditBox>();

                string outText;
                commentEditBox.Document.GetText(Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var comment = await StackOverflowSource.AddCommentAsync(this.root.Root.UserDetails.AccessToken, _answers[0].answer_id, outText);
                if (comment != null)
                {
                    FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
                    _comments.Add(comment.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].comments.Add(comment.items[0]);
                    _answers[0].comment_count = _comments.Count;
                    this.UpdateLayout();
                    this.mainScrollViewer.ChangeView(null, this.mainScrollViewer.ScrollableHeight, null);
                } // else will be an exception handling case. We'll handle exceptions later. By putting some info on the AppBar.
            }
            catch (Exception)
            {
            }
        }

        private void FlipCommentBox(Windows.UI.Xaml.Visibility state)
        {
            var commentBox = Utils.FindVisualChild<CommentBoxControl>(this.commentBoxControl);
            commentBox.Visibility = state;
        }

        private void addCommentButton_Click(object sender, EventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                FlipCommentBox(Windows.UI.Xaml.Visibility.Visible);
                this.UpdateLayout();
                var verticalOffset = this.mainScrollViewer.VerticalOffset + 200;
                this.mainScrollViewer.ChangeView(null, Math.Min(verticalOffset, this.mainScrollViewer.ScrollableHeight), null);
            }
        }

        private void TagButton_Click(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(GroupedItemsPage), (sender as Button).Content as string);
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            await StackOverflowSource.Instance.CheckForAnswerUpdate(_answerId, 0, root.Root.UserDetails.AccessToken);
            SetSources();
        }

        private void SetSources()
        {
            var answers = StackOverflowSource.Instance.AnswerDetailInfo.items.Where(x => x.answer_id == _answerId);
            _answers[0] = answers.First();
            _comments.AddRange(_answers.First().comments.EmptyIfNull(), clearCollection: true);
            this.AnswerSource.Source = _answers;
            this.AnswerComments.Source = _comments;
            this.UpdateLayout();
        }

        private void profileImageClick(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfilePage), _answers[0].owner.user_id.ToString());
        }
    }
}