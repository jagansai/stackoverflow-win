﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackOverflow.Common;
using StackOverflow.Data;
using Windows.ApplicationModel.Background;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using StackOverflow.Views;

// The Grouped Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234231

namespace StackOverflow
{

    enum SearchType
    {
        TagSearch,
        UserSearch
    }

    /// <summary>
    ///     A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class GroupedItemsPage : MainPageBase
    {
        public static GroupedItemsPage current;


        #region member fields

        private static bool _backButtonPressed = false;                
        private bool _timerStopped = false;
        private static bool _loggedIn = false;
        private static UserInfo _user = null;
        private static UserDetail _userDetail = null;

        public static readonly string _clientId = "4351";
        public static readonly string _redirectUrl = "https://stackexchange.com/oauth/login_success";

        
        #endregion


        public GroupedItemsPage()
        {
            InitializeComponent();
            current = this;
            Login();
            if (_user != null && _user.items != null)
            {
                _loggedIn = true;
                this.profilePanel.DataContext = _user.items[0];
                LoginButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        public static async void Login(bool login=false)
        {
            _userDetail = await Authenticator.Authenticate(_clientId, _redirectUrl);
        }
     

        public UserDetail UserDetails
        {
            get
            {
                return _userDetail;
            }
        }


        public void BackButtonPressed(bool pressed)
        {
            _backButtonPressed = pressed;
            if (pressed)
            {
                ScrollPosition = Views.ScrollPositions.ScrollToPrevious;
            }
        }

        void _questions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var incrementalLoader = sender as GeneratorIncrementalLoadingClass;
            if (incrementalLoader != null && incrementalLoader.DoneLoading)
            {
                SetScrollBar(_backButtonPressed ? ScrollPositions.ScrollToPrevious : ScrollPositions.ScrollToEnd); // Just use this to determine the resettoprev setting.
                if (_backButtonPressed)
                {
                    BackButtonPressed(false);
                }
            }           
        }

        protected override void SetScrollBar(Views.ScrollPositions position)
        {
            this.UpdateLayout();
            if (base.ScrollViewer == null)
            {
                base.ScrollViewer = this.itemsGrid.GetFirstDescendantOfType<ScrollViewer>();
            }
            base.SetScrollBar(position);
        }

        void scrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollEventType == ScrollEventType.EndScroll)
            {
                var bar = sender as ScrollBar;
                if (bar == null) return;

                if (bar.Minimum == e.NewValue)
                {                    
                    Questions.RemoveRange();
                }
            }
        }

        private void SetSources()
        {
            this.questionsViewSource.Source = Questions;
        }



       

        private bool AtBeginingOfPage()
        {
            return ScrollViewerHeight == 0.0;
        }

        /// <summary>
        ///     Invoked when "StackOverflow" header is clicked.
        private void Header_Click(object sender, RoutedEventArgs e)
        {
            Questions.RemoveRange();
            SetScrollBar(ScrollPositions.ScrollToBegin);
        }

        /// <summary>
        ///     Invoked when an item within a group is clicked.
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            InitTimerStop();
            int question_id = ((Question)e.ClickedItem).question_id;
            // int question_id = 1130516;
            // Analyze the question - 29315082
            var scrollviewer = Utils.FindVisualChild<ScrollViewer>(this.itemsGrid);
            ScrollViewerHeight = scrollviewer.HorizontalOffset;

            // save the data in StackOverflowSource.

            StackOverflowSource.Instance.SaveInfoData(Questions);
            this.Frame.Navigate(typeof(GroupDetailPage), question_id);
        }

        private void InitTimerStop()
        {
            _timerStopped = true;
            Timer.Stop();
        }
        private void InitTimerStart(int seconds)
        {
            Timer.Interval = new TimeSpan(0, 0, seconds);
            Timer.Start();
            _timerStopped = false;
        }

        private void backButtonHandler(object sender, BackClickEventArgs e)
        {
        }

        private void UpVoteButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void DownVoteButton_Click(object sender, RoutedEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="GridCS.Common.NavigationHelper.LoadState" />
        /// and
        /// <see cref="GridCS.Common.NavigationHelper.SaveState" />
        /// .
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            NavigationHelper.OnNavigatedTo(e);
            base.OnNavigatedTo(e);
            var askForReview = await MainPageBase.AskForReview();
            if (askForReview)
            {
               // Review_Click(null, null);
            }
        }

        #endregion

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void TagButton_Click(object sender, RoutedEventArgs e)
        {
            var textblock = (sender as Button).GetDescendantsOfType<TextBlock>().First();
            this.Frame.Navigate(typeof(GroupedItemsPage), textblock.Text);
        }

        private async void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            await Questions.LoadNewQuestionsAsync();
            this.updateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            InitTimerStart(20);
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void SearchBoxVisibility(Windows.UI.Xaml.Visibility state, SearchType searchType)
        {
            switch(state)
            {
                case Windows.UI.Xaml.Visibility.Visible : 
                switch(searchType)
                {
                    case SearchType.TagSearch:
                        tagSearchBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        break;
                    case SearchType.UserSearch:
                        userSearchBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        break;

                }
                break;

                case Windows.UI.Xaml.Visibility.Collapsed :
                switch (searchType)
                {
                    case SearchType.TagSearch:
                        tagSearchBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        break;
                    case SearchType.UserSearch:
                        userSearchBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        break;

                }
                break;                     
            }
        }

        private void TagSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Visible, SearchType.TagSearch); 
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch); 
        }

        private async void TagSearchEventsSuggestionsRequested(SearchBox sender, SearchBoxSuggestionsRequestedEventArgs args)
        {

            var deferral = args.Request.GetDeferral();
            try
            {
                if (args.QueryText != string.Empty)
                {
                    var tagInfo = await StackOverflowSource.Instance.GetTags(args.QueryText);
                    var tags = tagInfo.items.Select(x => x.name);
                    args.Request.SearchSuggestionCollection.AppendQuerySuggestions(tags);
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
            }
            finally
            {
                deferral.Complete();
            }
        }

        private void TagSearchEventsQuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch);
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch); 
            this.Frame.Navigate(typeof(GroupedItemsPage), args.QueryText);
        }

        private void TagSearchEventsResultSuggestionChosen(SearchBox sender, SearchBoxResultSuggestionChosenEventArgs args)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch); 
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch); 
        }

        private void UserSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch);
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Visible, SearchType.UserSearch); 
        }

        private async void UserSearchEventsSuggestionsRequested(SearchBox sender, SearchBoxSuggestionsRequestedEventArgs args)
        {
            var deferral = args.Request.GetDeferral();
            try
            {
                var userInfo = await StackOverflowSource.Instance.GetUsers(args.QueryText);
                foreach (var item in userInfo.items)
                {
                    args.Request.SearchSuggestionCollection.AppendResultSuggestion(item.display_name, item.reputation.ToString(), item.user_id.ToString(), Windows.Storage.Streams.RandomAccessStreamReference.CreateFromUri(new Uri(item.profile_image)), string.Empty);
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
            }
            catch (Exception)
            {
            }
            finally
            {
                deferral.Complete();
            }
        }

        private void UserSearchEventsQuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
        }

        private void UserSearchEventsResultSuggestionChosen(SearchBox sender, SearchBoxResultSuggestionChosenEventArgs args)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch); 
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch); 
            this.Frame.Navigate(typeof(UserProfilePage), args.Tag);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateUI();
        }

        private void InboxButton_Click(object sender, RoutedEventArgs e)
        {
            inboxCircle.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.Frame.Navigate(typeof(NotificationsPage));
        }

        private void profileImageClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfilePage), _user.items[0].user_id.ToString());
        }

        public override void SetDisplayItems(string param)
        {
          //  SetTagOnTileVisibility(param.NullOrEmpty() ? Visibility.Collapsed : Visibility.Visible, param);
            SetSources();
            InitTimerStart(20);
        }

        protected async override void UpdateUI()
        {
        if (!_timerStopped)
        {
            bool updateAvailable = Questions.NewQuestionsArrived();
            if (updateAvailable)
            {
                if (AtBeginingOfPage())
                {
                    await Questions.LoadNewQuestionsAsync();
                    InitTimerStart(20);
                }
                else
                {
                    updateReadyButton.Visibility = Visibility.Visible;
                    InitTimerStop();
                }
            }
            var newInboxItems = await StackOverflowSource.Instance.CheckForNewInboxItems(1, current.UserDetails.AccessToken);
            if (newInboxItems)
            {
                inboxCircle.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }
    }
    }
}
