﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using Windows.Storage;
using Windows.Storage.Streams;
using Newtonsoft.Json;
using System.IO;
using Windows.Web.Http.Filters;

namespace StackOverflow.Common
{

    public sealed class UserDetail
    {
        public string AccessToken { get; set; }
    }

    #if WINDOWS_PHONE_APP
    sealed class AuthenticatorPhone : IWebAuthenticationContinuable
    #endif

    sealed class Authenticator
    {

        private static readonly string _url = "https://stackexchange.com/oauth/?client_id=";
        private static readonly string _scope = "&scope=read_inbox,no_expiry,write_access,private_info&display=popup&response_type=token";

        public static async Task<UserDetail> Authenticate(string client_id, string redirect_url)
        {
            try
            {

                string stackOverflowUrl = _url + Uri.EscapeDataString(client_id) + "&redirect_uri=" + Uri.EscapeDataString(redirect_url) + _scope;
               //String stackOverflowUrl = "https://stackexchange.com/oauth/?client_id=" + Uri.EscapeDataString(client_id) + "&redirect_uri=" + Uri.EscapeDataString(redirect_url) + "&scope=read_inbox,no_expiry,write_access,private_info&display=popup&response_type=token";

                System.Uri startUri = new Uri(stackOverflowUrl);
                System.Uri endUri = new Uri(redirect_url);

#if WINDOWS_PHONE_APP
                WebAuthenticationBroker.AuthenticateAndContinue(startUri, endUri, null , WebAuthenticationOptions.None);
#else
                WebAuthenticationResult WebAuthenticationResult = await WebAuthenticationBroker.AuthenticateAsync(
                                                        WebAuthenticationOptions.None,
                                                        startUri,
                                                        endUri);
                if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.Success)
                {

                    return await GetStackUserInfoAsync(WebAuthenticationResult.ResponseData.ToString());
                }
                else if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
                {
                  string error = WebAuthenticationResult.ResponseErrorDetail.ToString();
                }
                else
                {
                    
                }
#endif
            }
            catch (Exception )
            {
            }
            return new UserDetail();
        }

        public async static Task<UserDetail> GetAccessTokenFromFile()
        {
#if WINDOWS_PHONE_APP
            return new UserDetail();
#else
            try
            {
                var file = await ApplicationData.Current.LocalFolder.GetFileAsync("UserDetails");
                using (var inStream = await file.OpenReadAsync())
                {
                    using (var textReader = new DataReader(inStream))
                    {
                        uint fileSize = (uint)inStream.Size;
                        await textReader.LoadAsync(fileSize);
                        string contents = textReader.ReadString(fileSize);
                        var userDetail = JsonConvert.DeserializeObject<UserDetail>(contents);
                        return userDetail;
                    }
                }
            }
            catch(Exception )
            {
                return new UserDetail();
            }
#endif
        }

        /// <summary>
        /// This function extracts access_token from the response returned from web authentication broker
        /// and uses that token to get user information using facebook graph api. 
        /// </summary>
        /// <param name="webAuthResultResponseData">responseData returned from AuthenticateAsync result.</param>
        private static async Task<UserDetail> GetStackUserInfoAsync(string webAuthResultResponseData)
        {
            //Get Access Token first
            string responseData = webAuthResultResponseData.Substring(webAuthResultResponseData.IndexOf("access_token"));
            String[] keyValPairs = responseData.Split('&');
            string access_token = null;
            string expires_in = null;
            for (int i = 0; i < keyValPairs.Length; i++)
            {
                String[] splits = keyValPairs[i].Split('=');
                switch (splits[0])
                {
                    case "access_token":
                        access_token = splits[1]; //you may want to store access_token for further use. Look at Scenario5 (Account Management).
                        break;
                    case "expires_in":
                        expires_in = splits[1];
                        break;
                }
            }

            // Save the access token to file.
#if WINDOWS_PHONE_APP
            return new UserDetail();
#else
            var userDetail = new UserDetail();
            try
            {
                var file = await ApplicationData.Current.LocalFolder.CreateFileAsync("UserDetails", CreationCollisionOption.ReplaceExisting);

                userDetail.AccessToken = access_token;
                await Windows.Storage.FileIO.WriteTextAsync(file,
                            JsonConvert.SerializeObject(userDetail));

                return userDetail; 
            }
            catch(Exception )
            {
                return userDetail;
            }
#endif
        }
    }
}
