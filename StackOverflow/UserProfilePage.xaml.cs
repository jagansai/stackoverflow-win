﻿using StackOverflow.Common;
using System;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;

// The Hub Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=321224

namespace StackOverflow
{


    internal class UserItem
    {
        public string ItemType { get; set; }
    }


    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class UserProfilePage : Page
    {
        private NavigationHelper navigationHelper;
        private readonly ObservableCollection<UserItem> _tabs = new ObservableCollection<UserItem>()
        {
            new UserItem() 
            {
                ItemType = "Questions"
            },
            
            new UserItem()
            {
                ItemType = "Answers"
            },
            new UserItem()
            {
                ItemType = "Favorites"
            }
        };

        private string UserId { get; set; }

        //private IncrementalLoadingAnswerClass _answers = null;
        //private IncrementalLoadingQuestionClass _questions = null;
        //private IncrementalLoadingFavClass _favorites = null;
        //private ScrollViewer _answersScroller = null;

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public UserProfilePage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
        }


        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            UserId = e.NavigationParameter as string;
            var userInfo = await QueryStackOverflow.GetUserInfoAsync(UserId);
            userProfileSource.Source = new ObservableCollection<User>(userInfo.items);
            var border = this.GetDescendantsOfType<Border>().Where(x => x.Name.Equals("profileDescBorder")).First();
            border.Visibility = Windows.UI.Xaml.Visibility.Visible;
            var userInfoList = this.GetFirstDescendantOfType<ListBox>();
        
            if (userInfoList != null)
            {
                userInfoList.ItemsSource = _tabs;
            }
            userInfoList.SelectedIndex = 0;
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion



        private async void userInfoList_ItemClick(object sender, SelectionChangedEventArgs e)
        {
            var info = (e.AddedItems.First() as UserItem);
            var gridViewControls = this.GetDescendantsOfType<GridView>();

            foreach (var item in gridViewControls)
            {
                item.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            if (info.ItemType.Equals("Answers"))
            {
                var answerInfo = await QueryStackOverflow.GetAnswersForUserAsync(UserId, 1, GroupedItemsPage.current.UserDetails.AccessToken);
                this.userAnswersSource.Source = answerInfo.items;
                var gridView = gridViewControls.Where(x => x.Name.Equals("answersGrid")).First();
                gridView.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else if (info.ItemType.Equals("Questions"))
            {
                var questionInfo = await QueryStackOverflow.GetQuestionsForUserAsync(UserId, 1, GroupedItemsPage.current.UserDetails.AccessToken);
                this.userQuestionsSource.Source = questionInfo.items;
                var gridView = gridViewControls.Where(x => x.Name.Equals("questionsGrid")).First();
                gridView.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else if (info.ItemType.Equals("Favorites"))
            {
                var favoritesInfo = await QueryStackOverflow.GetFavoritesForUserAsync(UserId, 1, GroupedItemsPage.current.UserDetails.AccessToken);
                this.userFavoritesSource.Source = favoritesInfo.items;
                var gridView = gridViewControls.Where(x => x.Name.Equals("favoritesGrid")).First();
                gridView.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        //private void Answers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    if (_answersScroller == null)
        //    {
        //        _answersScroller = _answersGridView.GetFirstDescendantOfType<ScrollViewer>();
        //    }
        //    var incrementalLoader = sender as IncrementalLoadingAnswerClass;
        //    if (incrementalLoader != null && incrementalLoader.DoneLoading)
        //    {
        //        this.UpdateLayout();

        //        _answersScroller.ChangeView(_answersScroller.HorizontalOffset, null, null);
        //        var scrollBars = _answersScroller.GetDescendantsOfType<ScrollBar>();
        //        var scrollBar = scrollBars.FirstOrDefault((x) => x.Orientation == Orientation.Horizontal);
        //        scrollBar.Scroll += scrollBar_Scroll; 
        //        //scrollViewer.ChangeView(scrollViewer.HorizontalOffset, null, null);
        //        _answers.LoadMore = false;                         
        //    }
        //}

        //private void scrollBar_Scroll(Object sender, ScrollEventArgs e)
        //{
        //    if (e.ScrollEventType == ScrollEventType.EndScroll)
        //    {
        //        _answers.LoadMore = false;
        //    }
        //    else
        //    {
        //        _answers.LoadMore = true;
        //    }
        //}

        private void FavouriteItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Question)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }

        private void AnswerItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Answer)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }

        private void QuestionItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Question)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }
    }
}
