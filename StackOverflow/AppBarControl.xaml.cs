﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace StackOverflow
{
    public sealed partial class AppBarControl : UserControl
    {
        public AppBarControl()
        {
            this.InitializeComponent();
        }

        public event EventHandler TagBtnClick;
        public event EventHandler BackBtnClick;
        public event EventHandler UpdateReadyBtnClick;

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            BackBtnClick(sender, EventArgs.Empty);
        }

        private void TagButton_Click(object sender, EventArgs e)
        {
            TagBtnClick(sender, EventArgs.Empty);
        }

        private void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateReadyBtnClick(sender, EventArgs.Empty);
        }
    }
}
