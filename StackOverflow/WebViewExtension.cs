﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace StackOverflow
{
    class WebViewExtension
    {
        public static string Getbody(DependencyObject obj)
        {
            return (string)obj.GetValue(HTMLProperty);
        }

        public static void Setbody(DependencyObject obj, string value)
        {
            obj.SetValue(HTMLProperty, value);
        }

        // Using a DependencyProperty as the backing store for HTML.  This enables animation, styling, binding, etc... 
        public static readonly DependencyProperty HTMLProperty =
            DependencyProperty.RegisterAttached("body", typeof(string), typeof(WebViewExtension), new PropertyMetadata(0,new PropertyChangedCallback(OnHTMLChanged)));

        private static void OnHTMLChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            WebView wv = d as WebView;
            if (wv != null)
            {
                wv.NavigateToString(e.NewValue as string);
            }
        }

    }
}

