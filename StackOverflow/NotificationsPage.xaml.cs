﻿using StackOverflow.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using StackOverflow.Data;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace StackOverflow
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class NotificationsPage : Page
    {

        private NavigationHelper navigationHelper;

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public NotificationsPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

       
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            var inboxInfo = await StackOverflowSource.Instance.LoadInboxItems(1, GroupedItemsPage.current.UserDetails.AccessToken);
            inboxSource.Source = inboxInfo.items;
        }

        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void inboxItem_Click(object sender, ItemClickEventArgs e)
        {
            var inboxItem = e.ClickedItem as Inbox;
            if (inboxItem.item_type.Equals("comment"))
            {
                if (inboxItem.question_id == 0) // then comment on answer.
                {
                    var questionInfo = await QueryStackOverflow.QueryQuestionWithAnswerIdAsync(inboxItem.answer_id);
                    this.Frame.Navigate(typeof(GroupDetailPage), questionInfo.items[0].question_id);
                }
                else // comment on question.
                {
                    this.Frame.Navigate(typeof(GroupDetailPage), inboxItem.question_id);
                }
            }
        }

    }
}
