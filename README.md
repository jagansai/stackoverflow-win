# README #

A StackOverflow client on Windows and Windows Mobile. A new version of the app is being built that runs on Windows, Windows Mobile and Android.

### What is this repository for? ###

* This is the complete source code of StackCook, the StackOverflow client for Windows and Windows Mobile.
* [Find the app here](https://www.microsoft.com/en-US/store/Apps/StackCook/9WZDNCRD202N)

### How do I get set up? ###

* Download the source code on Windows 8.1 or higher.
* Install Visual Studio 2013 or higher and bring up the solution.

