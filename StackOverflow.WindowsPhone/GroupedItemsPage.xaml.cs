﻿using StackOverflow.Common;
using StackOverflow.Data;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace StackOverflow
{

    enum ScrollPositions
    {
        ScrollToPrevious,
        ScrollToBegin,
        ScrollToEnd,
        ScrollNormal // Just stay there. No jumping here and there :-)
    }

    enum SearchType
    {
        TagSearch,
        UserSearch
    }


    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GroupedItemsPage : Page
    {
        private readonly NavigationHelper _navigationHelper;
        public static GroupedItemsPage current;

        private DispatcherTimer _timer;



        #region member fields

        private GeneratorIncrementalLoadingClass _questions = null;
        private readonly int _maxPages = 30;
        //private ScrollViewer _scrollViewer = null;
        private static double _scrollViewerHeight = 0.0; // maintain the previous height.        
        private bool _timerStopped = false;
        private static bool _loggedIn = false;
        private static ObservableCollection<User> _users = new ObservableCollection<User>();
        private static UserDetail _userDetail = new UserDetail();
        ScrollPositions _scrollPositions = ScrollPositions.ScrollNormal;
        private static bool _tileOn = false;
        private static bool _backgroundTaskReg = false;
        private bool _updateInProgress = false;
        #endregion


        public GroupedItemsPage()
        {
            InitializeComponent();
            InitItems();
            _navigationHelper = new NavigationHelper(this);
            Window.Current.Activated += Current_Activated;
            _navigationHelper.LoadState += navigationHelper_LoadState;
            _navigationHelper.SaveState += navigationHelper_SaveState;
            App.Current.Suspending += Current_Suspending;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            ApplicationView.TerminateAppOnFinalViewClose = true;
        }

        private void InitItems()
        {
            _timer = new DispatcherTimer();
            _timer.Tick += (s, ev) => UpdateUI();
            current = this;

        }


        private async void Current_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            try
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                await SuspensionManager.SaveAsync();
                deferral.Complete();
            }
            catch (Exception)
            {
                // eat the exception. Do Nothing.
                Application.Current.Exit();
            }
        }

        private static async void SendQuestionsToTileManually(string tag)
        {
            var questionsInfo = await QueryStackOverflow.QueryTaggedQuestions(QueryStackOverflow.site_, tag.NullOrEmpty() ? string.Empty : tag, 1, 5);
            if (questionsInfo.items != null)
            {
                foreach (var q in questionsInfo.items)
                {
                    SOQuestionNotifier.MonitorQuesAndInbox.SendTileTextNotification(q.title, q.creation_date);
                }
                //   SendBadgeNotification(questionsInfo.items.Count);
            }
        }

        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {

        }

        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            if (NavigationHelper.CanGoBack())
            {
                tagSearchBox.Visibility = Visibility.Collapsed;
                userSearchBox.Visibility = Visibility.Collapsed;
                _scrollPositions = ScrollPositions.ScrollToPrevious;
            }
            else
            {
                Application.Current.Exit();
            }
        }

        void Current_Activated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            Window.Current.Activated -= Current_Activated;
            this.usersViewSource.Source = _users;
            //Login(atLogin: true);
        }

        private static async Task GetLoginDetailsFromFile()
        {
            if (_userDetail == null)
            {
                _userDetail = await AuthenticatorPhone.GetAccessTokenFromFile();
                if (_userDetail != null)
                {
                    await GetUserInfo();
                }
            }
        }

        public static async void Login(bool atLogin=false)
        {
            if (!_loggedIn)
            {
                await GetLoginDetailsFromFile();
                if (_userDetail == null || _userDetail.AccessToken.NullOrEmpty())
                {
                    // client_id = 4351. Store this is App.xaml
                    AuthenticatorPhone.Instance.Authenticate("4351", "https://stackexchange.com/oauth/login_success");
                }
                else
                {
                    await GetUserInfo();
                }
            }
        }

        // Called once the authenticator is success.
        public async void SetUserDetails(UserDetail userDetail)
        {
            _userDetail = userDetail;
            await GetUserInfo();
            this.usersViewSource.Source = _users.ToList();
        }
        
        private static async Task GetUserInfo()
        {
            _loggedIn = true;
            if (_users.Count == 0)
            {
                var userInfo = await QueryStackOverflow.QueryUserInfoAsync(_userDetail.AccessToken, QueryStackOverflow.site_);
                _users.AddRange(userInfo.items);                
            }
        }


        public UserDetail UserDetails
        {
            get
            {
                return _userDetail;
            }
        }

        /// <summary>
        ///     NavigationHelper is used on each page to aid in navigation and
        ///     process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return _navigationHelper; }
        }

        public static bool TileOn
        {
            get
            {
                return _tileOn;
            }

            set
            {
                _tileOn = value;

                if (!_tileOn)
                {
                    BackgroundTask.UnregisterBackgroundTasks(BackgroundTask.SampleBackgroundTaskName);
                }
                else
                {
                    RegisterBackgroundTask(string.Empty);
                }
            }
        }

        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // Handle back button here. Try not to hit the API when the back button is called.
            InitItems();
            string param = e.NavigationParameter as string;
            _questions = new GeneratorIncrementalLoadingClass(_maxPages, param);
            MakeProgressActive();
            SetSources();
            if (TileOn && !_backgroundTaskReg)
            {
                RegisterBackgroundTask(param);
            }

            if (_loggedIn)
            {
                loginButton.Visibility = Visibility.Collapsed;
            }

            //if (param.Equals("Back"))
            //{
            //    SetTagOnTileVisibility(Visibility.Collapsed);
            //    InitTimerStart(20);
            //}
            else if (!param.NullOrEmpty())
            {
                SetTagOnTileVisibility(Visibility.Visible, param);
            }
            else // First time.
            {
                SetTagOnTileVisibility(Visibility.Collapsed);
            }
            await _questions.LoadMoreItemsAsync();
            InitTimerStart(20);
        }

        private static void RegisterBackgroundTask(string param)
        {
            BackgroundTask.UnregisterBackgroundTasks(BackgroundTask.SampleBackgroundTaskName);
            var task = BackgroundTask.RegisterBackgroundTask(BackgroundTask.SampleBackgroundTaskEntryPoint,
                                                                              BackgroundTask.SampleBackgroundTaskName,
                                                                              new TimeTrigger(15, false), null);
            _backgroundTaskReg = true;
            SendQuestionsToTileManually(param);
        }

        private void SetTagOnTileVisibility(Visibility state, string param = "")
        {
            tagOnTile.Visibility = state;
            if (!param.NullOrEmpty())
            {
                tagOnTile.Label = string.Format("Pin {0} on Tile", param);
            }
        }

        void _questions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var incrementalLoader = sender as GeneratorIncrementalLoadingClass;
            if (incrementalLoader != null && incrementalLoader.DoneLoading)
            {
                SetScrollBar(_scrollPositions); // Just use this to determine the resettoprev setting.
                MakeProgressPassive();
            }
        }

        private void SetScrollBar(ScrollPositions position)
        {
            this.UpdateLayout();

            //if (_scrollViewer == null)
            //{
            //    _scrollViewer = this.itemListView.GetFirstDescendantOfType<ScrollViewer>();
            //}
            //if (_scrollViewer != null)
            //{
                if (position == ScrollPositions.ScrollToBegin)
                {
                    ScrollToBegin();
                }
                else if (position == ScrollPositions.ScrollToEnd)
                {
                    _scrollViewerHeight = SV1.VerticalOffset;
                }
                else if (position == ScrollPositions.ScrollToPrevious)
                {
                    SV1.ChangeView(null, _scrollViewerHeight, null);
                    _scrollPositions = ScrollPositions.ScrollNormal; //
                }
                var scrollBars = SV1.GetDescendantsOfType<ScrollBar>();
                var scrollBar = scrollBars.FirstOrDefault((x) => x.Orientation == Orientation.Vertical);
                scrollBar.Scroll += scrollBar_Scroll;
            //}
        }

        void scrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollEventType == ScrollEventType.EndScroll)
            {
                var bar = sender as ScrollBar;
                if (bar == null) return;

                if (bar.Minimum == e.NewValue)
                {
                    _questions.RemoveRange();
                }
            }
        }

        private void SetSources()
        {

            this.questionsViewSource.Source = _questions;
            _questions.CollectionChanged += _questions_CollectionChanged;
            this.usersViewSource.Source = _users.ToList();
            this.UpdateUI(); 

        }

        private async void UpdateUI()
        {
            if (!_timerStopped)
            {
                bool updateAvailable = _questions.NewQuestionsArrived();
                if (updateAvailable)
                {
                    updateReadyCircle.Visibility = Visibility.Visible;
                    InitTimerStop();
                }
                var newInboxItems = await StackOverflowSource.Instance.CheckForNewInboxItems(1, current.UserDetails.AccessToken);
                if (newInboxItems)
                {
                    inboxUnReadCircle.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
            }
        }

        private bool AtBeginingOfPage()
        {
            return _scrollViewerHeight == 0.0;
        }

        /// <summary>
        ///     Invoked when an item within a group is clicked.
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            StackOverflowSource.Instance.SaveInfoData(_questions);
            int question_id = ((Question)e.ClickedItem).question_id;
            _scrollViewerHeight = SV1.VerticalOffset;
            this.Frame.Navigate(typeof(GroupDetailPage), question_id);
        }

        private void InitTimerStop()
        {
            _timerStopped = true;
            _timer.Stop();
        }
        private void InitTimerStart(int seconds)
        {
            _timer.Interval = new TimeSpan(0, 0, seconds);
            _timer.Start();
            _timerStopped = false;
        }

        private void backButtonHandler(object sender, BackClickEventArgs e)
        {
        }

        private void UpVoteButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void DownVoteButton_Click(object sender, RoutedEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="GridCS.Common.NavigationHelper.LoadState" />
        /// and
        /// <see cref="GridCS.Common.NavigationHelper.SaveState" />
        /// .
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedTo(e);
            base.OnNavigatedTo(e);
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TagButton_Click(object sender, RoutedEventArgs e)
        {
            var textBlock = (sender as Button).GetFirstDescendantOfType<TextBlock>();
            this.Frame.Navigate(typeof(GroupedItemsPage), textBlock.Text);
        }

        private async void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            _updateInProgress = true;
            await GetNewUpdates();
            _updateInProgress = false;
        }

        private async Task GetNewUpdates()
        {
            ScrollToBegin();
            MakeProgressActive();
            if (updateReadyCircle.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                await _questions.LoadNewQuestionsAsync();
                this.updateReadyCircle.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                InitTimerStart(20);
            }
            else
            {
                // bring the scrollviewer to top, load new questions.
                bool newQuestions = _questions.NewQuestionsArrived();
                if (newQuestions)
                {
                    await _questions.LoadNewQuestionsAsync();
                    InitTimerStart(20);
                }
                else
                {
                    _questions.RemoveRange();
                }
            }
            MakeProgressPassive();
        }

        private void MakeProgressPassive()
        {
            updateRing.Visibility = Visibility.Collapsed;
            updateRing.IsActive = false;
        }

        private void MakeProgressActive()
        {
            updateRing.Visibility = Visibility.Visible;
            updateRing.IsActive = true;
        }

        private void ScrollToBegin()
        {
            SV1.ChangeView(null, 0, null);
        }



        private void SearchBoxVisibility(Windows.UI.Xaml.Visibility state, SearchType searchType)
        {
            switch (state)
            {
                case Windows.UI.Xaml.Visibility.Visible:
                    switch (searchType)
                    {
                        case SearchType.TagSearch:
                            tagSearchBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            tagSearchBox.Items.Clear();
                            break;
                        case SearchType.UserSearch:
                            userSearchBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            userSearchBox.Text = string.Empty;
                            break;

                    }
                    break;

                case Windows.UI.Xaml.Visibility.Collapsed:
                    switch (searchType)
                    {
                        case SearchType.TagSearch:
                            tagSearchBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                            break;
                        case SearchType.UserSearch:
                            userSearchBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                            break;

                    }
                    break;
            }
        }

        private void TagSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Visible, SearchType.TagSearch);
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch);
        }


        private async void tagSearch_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                try
                {
                    var tagInfo = await StackOverflowSource.Instance.GetTags(sender.Text.ToLower());
                    tagSearchBox.ItemsSource = tagInfo.items.Select(x => x.name);
                }
                catch (System.Threading.Tasks.TaskCanceledException)
                {
                    tagSearchBox.Items.Clear();
                }
                catch (Exception)
                {
                    tagSearchBox.Items.Clear();
                }
                finally
                {
                }
            }
        }

        private void tagSearch_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            this.Frame.Navigate(typeof(GroupedItemsPage), args.SelectedItem as string);
        }

        private void tagSearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SearchBoxVisibility(Visibility.Collapsed, SearchType.TagSearch);
        }

        private async void userSearch_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                try
                {
                    string text = sender.Text;
                    if (text.Length >= 3)
                    {
                        var userInfo = await StackOverflowSource.Instance.GetUsers(sender.Text.ToLower());
                        userSearchBox.ItemsSource = userInfo.items;
                    }
                }
                catch (System.Threading.Tasks.TaskCanceledException)
                {
                    userSearchBox.Items.Clear();
                }
                catch (Exception)
                {
                    userSearchBox.Items.Clear();
                }
                finally
                {
                }
            }
        }

        private void userSearch_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            var user = args.SelectedItem as User;
            this.Frame.Navigate(typeof(UserProfile), user.user_id.ToString());
        }

        private void userSearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //userSearchBox.Items.Clear();
            //tagSearchBox.Items.Clear();
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch);
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.UserSearch);
        }

        private void UserSearchButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed, SearchType.TagSearch);
            SearchBoxVisibility(Windows.UI.Xaml.Visibility.Visible, SearchType.UserSearch);
        }

        private void Inbox_Click(object sender, RoutedEventArgs e)
        {
            this.inboxUnReadCircle.Visibility = Visibility.Collapsed;
            this.Frame.Navigate(typeof(InboxPage));
        }

        private async void Refresh_Click(object sender, RoutedEventArgs e)
        {
            _questions.Tags = string.Empty;
            _questions.Clear();
            await _questions.LoadNewQuestionsOnDemandAsync();
        }

        private async void Review_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.Current.LocalSettings.Values["askforreview"] = false;
            ApplicationData.Current.LocalSettings.Values["started"] = 11;
            const string appId = "9c34fd1b-a44f-4b5b-8932-c7c63db66bcb";
            await Windows.System.Launcher.LaunchUriAsync(new Uri(string.Format("ms-windows-store:reviewapp?appid={0}", appId)));
        }

        private void ProfileImage_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfile), _users[0].user_id.ToString());
        }

        private void TagOnTile_Click(object sender, RoutedEventArgs e)
        {
            string temp = tagOnTile.Label;
            var start = temp.IndexOf(' ');
            var end = temp.IndexOf("on") - 2;
            RegisterBackgroundTask(temp.Substring(start + 1, end - start));
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Settings));
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login(atLogin: false);
        }

        private void QuestionSearch_Click(object sender, RoutedEventArgs e)
        {
        }

        private async void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer sv = sender as ScrollViewer;

            if (sv.VerticalOffset == 0 && !_updateInProgress)
            {
                await GetNewUpdates();
            }
            else if (sv.VerticalOffset == sv.ScrollableHeight)
            {
                MakeProgressActive();
                _questions.ScrollMore = true;
                await _questions.LoadMoreItemsAsync();
            }
        }
    }
}
