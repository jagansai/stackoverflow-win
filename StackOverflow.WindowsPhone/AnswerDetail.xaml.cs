﻿using StackOverflow.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using StackOverflow.Data;
using Windows.ApplicationModel.DataTransfer;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace StackOverflow
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AnswerDetail : SharePage
    {
        private NavigationHelper navigationHelper;
        private GroupDetailPage root = GroupDetailPage.current;
        private int _answerId = 0;



        private ObservableCollection<Answer> _answers = new ObservableCollection<Answer>();
        private ObservableCollection<Comment> _comments = new ObservableCollection<Comment>();
        private DispatcherTimer _timer = new DispatcherTimer()
        {
            Interval = new TimeSpan(0, 0, 20)
        };

        public AnswerDetail()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            var virtualKeyboard = Windows.UI.ViewManagement.InputPane.GetForCurrentView();
            virtualKeyboard.Showing += virtualKeyboard_Showing;
            virtualKeyboard.Hiding += virtualKeyboard_Hiding;
        }

        private void virtualKeyboard_Hiding(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            var trans = new TranslateTransform();
            trans.Y = 0;
            this.RenderTransform = trans;
            args.EnsuredFocusedElementInView = false;
        }

        private void virtualKeyboard_Showing(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            int offSet = (int)args.OccludedRect.Height;
            args.EnsuredFocusedElementInView = true;
            var trans = new TranslateTransform();
            trans.Y = -offSet;
            this.RenderTransform = trans;
        }

        private void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            _answerId = (int)e.NavigationParameter;
            _answers.AddRange(StackOverflowSource.Instance.DetailInfo.items[0].answers.Where(a => a.answer_id == _answerId));
            SetSources();
            _timer.Tick += (s, ev) => CheckForUpdates(s, ev, _answerId);
        }

        private async void CheckForUpdates(object s, object ev, object _questionId)
        {
            try
            {
                bool updateAvailable = await StackOverflowSource.Instance.CheckForAnswerUpdate(_answerId, _answers[0].last_activity_date, GroupedItemsPage.current.UserDetails.AccessToken);
                if (updateAvailable)
                {
                    updateReadyButton.Visibility = Visibility.Visible;
                }
            }
            catch(Exception)
            {

            }
        }

        private void SetSources()
        {
            AnswerViewSource.Source = _answers;
            QuestionViewSource.Source = StackOverflowSource.Instance.DetailInfo.items;
            _comments.AddRange(_answers[0].comments.EmptyIfNull());
            CommentsViewSource.Source = _comments;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.navigationHelper.OnNavigatedFrom(e);
        }

        protected override bool GetShareContent(DataRequest request)
        {
            bool succeeded = false;
            Uri dataPackageUri = new Uri(StackOverflowSource.Instance.DetailInfo.items[0].link);
            DataPackage requestData = request.Data;
            requestData.Properties.Title = StackOverflowSource.Instance.DetailInfo.items[0].title;
            requestData.Properties.ContentSourceApplicationLink = dataPackageUri;
            requestData.SetWebLink(dataPackageUri);
            succeeded = true;
            return succeeded;
        }

        #endregion

        private async void commentsubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                commentBoxControl.IsEnabled = false;
                var commentEditBox = commentBoxControl.GetFirstDescendantOfType<RichEditBox>();

                string outText;
                commentEditBox.Document.GetText(Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var comment = await StackOverflowSource.AddCommentAsync(root.Root.UserDetails.AccessToken, _answers[0].answer_id, outText);
                if (comment != null)
                {
                    FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
                    _comments.Add(comment.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].comments.Add(comment.items[0]);
                    _answers[0].comment_count = _comments.Count;
                    this.UpdateLayout();
                    mainScrollViewer.ChangeView(null, mainScrollViewer.ScrollableHeight, null);
                } // else will be an exception handling case. We'll handle exceptions later. By putting some info on the AppBar.
            }
            catch (Exception )
            {
            }
        }

        private void FlipCommentBox(Windows.UI.Xaml.Visibility state)
        {
            commentBoxControl.Visibility = state;
        }

        private void addCommentButton_Click(object sender, EventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login(false);
            }
            else
            {
                FlipCommentBox(Windows.UI.Xaml.Visibility.Visible);
                this.UpdateLayout();
                var verticalOffset = mainScrollViewer.VerticalOffset + 200;
                mainScrollViewer.ChangeView(null, Math.Min(verticalOffset, mainScrollViewer.ScrollableHeight), null);
            }
        }

        private async void UpVoteQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                if (!_answers[0].upvoted)
                {
                    var info = await StackOverflowSource.SetUpVote<AnswerInfo>(_answers[0].answer_id, root.Root.UserDetails.AccessToken);
                    UpVoteButton.IsEnabled = false;
                    _answers[0].score = info.items[0].score;
                    _answers[0].upvoted = info.items[0].upvoted;
                    this.UpdateLayout();
                }
            }
        }

        private async void DownVoteQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                if (!_answers[0].downvoted)
                {
                    var info = await StackOverflowSource.SetUpVote<AnswerInfo>(_answers[0].answer_id, root.Root.UserDetails.AccessToken);
                    DownVoteButton.IsEnabled = false;
                    _answers[0].score = info.items[0].score;
                    _answers[0].upvoted = info.items[0].downvoted;
                    this.UpdateLayout();
                }
            }
        }

        private void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            _answers.AddRange(StackOverflowSource.Instance.AnswerDetailInfo.items, clearCollection: true);
            SetSources();
        }

        private void TagButton_Click(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(GroupedItemsPage), (sender as Button).Content as string);
        }

        private void commentCancelButton_Click(object sender, EventArgs e)
        {
            FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            bool updateAvailable = await StackOverflowSource.Instance.CheckForAnswerUpdate(_answerId, _answers[0].last_activity_date, GroupedItemsPage.current.UserDetails.AccessToken);
            if (updateAvailable)
            {
                _answers.AddRange(StackOverflowSource.Instance.AnswerDetailInfo.items, clearCollection: true);
                SetSources();
            }
        }

        private void answerProfileImageClick(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfile), _answers[0].owner.user_id.ToString());
        }

        private void Share_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }
    }
}
