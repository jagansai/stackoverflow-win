﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using StackOverflow.Common;
using System.Threading.Tasks;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace StackOverflow
{

    internal class UserItem
    {
        public string ItemType { get; set; }
    }

    enum PivotIndex
    {
        QuestionPivot = 0,
        AnswerPivot = 1,
        FavoritePivot = 2
    }


    public sealed partial class UserProfile : Page
    {
        private const string Questions = "Questions";
        private const string Answers = "Answers";
        private const string Favorites = "Favorites";

        private int _questionPageIndex = 0;
        private int _answerPageIndex = 0;
        private int _favPageIndex = 0;


        private ObservableCollection<Question> _questions = null;
        private ObservableCollection<Answer> _answers = null;
        private ObservableCollection<Question> _favorties = null;

        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private string _userId = string.Empty;


        //private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");

        public UserProfile()
        {
            this.InitializeComponent();

            _questions = new ObservableCollection<Question>();
            _answers = new ObservableCollection<Answer>();
            _favorties = new ObservableCollection<Question>();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }


        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>.
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

            _userId = (e.NavigationParameter as string);
            var userInfo = await QueryStackOverflow.GetUserInfoAsync(_userId);
            userProfileSource.Source = new ObservableCollection<User>(userInfo.items);
            PivotItems.SelectedIndex = (int)PivotIndex.QuestionPivot; // Question Pivot.
        }



        private T DoWorkAndIgnoreException<T>(Func<T> a) where T : new()
        {
            try
            {
                return a();
            }
            catch (Exception)
            {
                return new T();
            }
        }

        private async Task LoadAnswerItemsAsync(string userId, int index)
        {
            try
            {
                var answerInfo = await QueryStackOverflow.GetAnswersForUserAsync(userId, index, GroupedItemsPage.current.UserDetails.AccessToken);
                _answers.Clear();
                _answers.AddRange(answerInfo.items);
                this.userAnswersSource.Source = _answers;
            }
            catch (Exception)
            { }
        }

        private async Task LoadQuestionItemsAsync(string userId, int index)
        {
            try
            {
                var questionInfo = await QueryStackOverflow.GetQuestionsForUserAsync(userId, index, GroupedItemsPage.current.UserDetails.AccessToken);
                _questions.Clear();
                _questions.AddRange(questionInfo.items);
                this.userQuestionsSource.Source = _questions;
            }
            catch (Exception)
            { }
        }

        private async Task LoadFavItemsAsync(string userId, int index)
        {
            try
            {
                var favoritesInfo = await QueryStackOverflow.GetFavoritesForUserAsync(userId, index, GroupedItemsPage.current.UserDetails.AccessToken);
                _favorties.Clear();
                _favorties.AddRange(favoritesInfo.items);
                this.userFavoritesSource.Source = _favorties;
            }
            catch (Exception)
            { }
        }


        private async void AnswerPivot_Loaded(object sender, RoutedEventArgs e)
        {
            string tempUserId = _userId;
            int index = ++_answerPageIndex;
            await LoadAnswerItemsAsync(tempUserId, index);
        }

        private async void FavoritesPivot_Loaded(object sender, RoutedEventArgs e)
        {
            string tempUserId = _userId;
            int index = ++_questionPageIndex;
            await LoadFavItemsAsync(tempUserId, index);
        }

        private async void QuestionsPivot_Loaded(object sender, RoutedEventArgs e)
        {
            string tempUserId = _userId;
            int index = ++_favPageIndex;
            await LoadQuestionItemsAsync(tempUserId, index);
        }


        private void QuestionItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Question)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }

        private void AnswerItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Answer)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }

        private void FavouriteItem_Click(object sender, ItemClickEventArgs e)
        {
            int questionId = ((Question)e.ClickedItem).question_id;
            this.Frame.Navigate(typeof(GroupDetailPage), questionId);
        }


        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        private async void LoadMore_Click(object sender, RoutedEventArgs e)
        {

            if (PivotItems.SelectedIndex == (int)PivotIndex.QuestionPivot)
            {
                string tempUserId = _userId;
                int index = ++_questionPageIndex;
                await LoadQuestionItemsAsync(tempUserId, index);
            }
            else if (PivotItems.SelectedIndex == (int)PivotIndex.AnswerPivot)
            {
                string tempUserId = _userId;
                int index = ++_answerPageIndex;
                await LoadAnswerItemsAsync(tempUserId, index);
            }
            else if (PivotItems.SelectedIndex == (int)PivotIndex.FavoritePivot)
            {
                string tempUserId = _userId;
                int index = ++_favPageIndex;
                await LoadFavItemsAsync(tempUserId, index);
            }
        }
    }
}
