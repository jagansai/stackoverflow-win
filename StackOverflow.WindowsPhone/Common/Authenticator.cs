﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Email;
using Windows.Security.Authentication.Web;
using Windows.Storage;
using Windows.Storage.Streams;

namespace StackOverflow.Common
{

    public sealed class UserDetail 
    {
        public string AccessToken { get; set; }
    }

    public sealed class UserSettings
    {
        public bool LiveTileOn { get; set; }
    }

    sealed class AuthenticatorPhone : IWebAuthenticationContinuable
    {
        UserDetail _userDetail;
        private static readonly AuthenticatorPhone _instance = new AuthenticatorPhone();

        private AuthenticatorPhone()
        {
        }

        public static AuthenticatorPhone Instance
        {
            get
            {
                return _instance;
            }
        }

        public void Authenticate(string client_id, string redirect_url)
        {
            try
            {
                String stackOverflowUrl = "https://stackexchange.com/oauth/?client_id=" + Uri.EscapeDataString(client_id) + "&redirect_uri=" + Uri.EscapeDataString(redirect_url) + "&scope=read_inbox,no_expiry,write_access,private_info&display=popup&response_type=token";
                System.Uri startUri = new Uri(stackOverflowUrl);
                System.Uri endUri = new Uri(redirect_url);
                WebAuthenticationBroker.AuthenticateAndContinue(startUri, endUri, null, WebAuthenticationOptions.None);
            }
            catch (Exception ex)
            {
                SendErrorReport(ex.StackTrace);
            }
        }

        private async void SendErrorReport(string error)
        {
            var md = new Windows.UI.Popups.MessageDialog("Thank you for using StackCook.", "Please review my app");
            bool? reviewresult = null;
            md.Commands.Add(new Windows.UI.Popups.UICommand("OK", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = true)));
            md.Commands.Add(new Windows.UI.Popups.UICommand("Cancel", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = false)));
            await md.ShowAsync();
            if (reviewresult == true)
            {
                EmailRecipient sendTo = new EmailRecipient()
                {
                    Address = "appkitchen@outlook.com"
                };
                EmailMessage mail = new EmailMessage();
                mail.Subject = "Crash Report";
                mail.Body = error;
                await EmailManager.ShowComposeNewEmailAsync(mail);
            }
        }

        public static Task<UserDetail> GetAccessTokenFromFile()
        {
            return GetDataFromFile<UserDetail>("UserDetails");
        }

        private async static Task<T> GetDataFromFile<T>(string fileName) where T : new()
        {
            try
            {
                var file = await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                using (var inStream = await file.OpenReadAsync())
                {
                    using (var textReader = new DataReader(inStream))
                    {
                        uint fileSize = (uint)inStream.Size;
                        await textReader.LoadAsync(fileSize);
                        string contents = textReader.ReadString(fileSize);
                        var obj = JsonConvert.DeserializeObject<T>(contents);
                        return obj;
                    }
                }
            }
            catch (Exception)
            {
                return new T();
            }
        }

        public async static Task<bool> GetUserSettings()
        {
            var userSettings = await GetDataFromFile<UserSettings>("UserSettings");
            return userSettings.LiveTileOn;
        }

        public async static Task SetUserSettings(UserSettings userSettings)
        {
            await WriteDataToFile<UserSettings>("UserSettings", userSettings);
        }


        private async static Task WriteDataToFile<T>(string filename, T t) where T : new ()
        {
            try
            {
                var file = await ApplicationData.Current.LocalFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                await Windows.Storage.FileIO.WriteTextAsync(file,
                            JsonConvert.SerializeObject(t));
            }
            catch (Exception)
            {
            }
        }



        public async void ContinueWebAuthentication(WebAuthenticationBrokerContinuationEventArgs args)
        {
            WebAuthenticationResult result = args.WebAuthenticationResult;


            if (result.ResponseStatus == WebAuthenticationStatus.Success)
            {
                //OutputToken(result.ResponseData.ToString());
                //await GetFacebookUserNameAsync(result.ResponseData.ToString());
               _userDetail = await GetStackUserInfoAsync(result.ResponseData.ToString());
               GroupedItemsPage.current.SetUserDetails(_userDetail);
            }
            else if (result.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
            {
                //OutputToken("HTTP Error returned by AuthenticateAsync() : " + result.ResponseErrorDetail.ToString());
            }
            else
            {
                //OutputToken("Error returned by AuthenticateAsync() : " + result.ResponseStatus.ToString());
            }
        }

        private static async Task<UserDetail> GetStackUserInfoAsync(string webAuthResultResponseData)
        {
            //Get Access Token first
            string responseData = webAuthResultResponseData.Substring(webAuthResultResponseData.IndexOf("access_token"));
            String[] keyValPairs = responseData.Split('&');
            string access_token = null;
            string expires_in = null;
            for (int i = 0; i < keyValPairs.Length; i++)
            {
                String[] splits = keyValPairs[i].Split('=');
                switch (splits[0])
                {
                    case "access_token":
                        access_token = splits[1]; //you may want to store access_token for further use. Look at Scenario5 (Account Management).
                        break;
                    case "expires_in":
                        expires_in = splits[1];
                        break;
                }
            }

            var userDetail = new UserDetail();
            userDetail.AccessToken = access_token;
            await WriteDataToFile<UserDetail>("UserDetails", userDetail);
            return userDetail;
        }
    }
}
