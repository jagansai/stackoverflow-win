﻿using StackOverflow.Data;
using StackOverflow.Views;
using System;
using System.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The Group Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234229

namespace StackOverflow
{
    /// <summary>
    /// A page that displays an overview of a single group, including a preview of the items
    /// within the group.
    /// </summary>

    /*
     *  Bugs
     *  1) Refreshing the data is not single step.
     *  2) Loading update is making the UI without any data.
     *  3) Code needs refactoring.
     * 
     */

    public sealed partial class GroupDetailPage : QuestionDetailPageBase
    {
        //private NavigationHelper navigationHelper;
        //DispatcherTimer _timer = null;
        bool _timerStopped;
        
        public static GroupDetailPage current;


        private readonly HubSection _questionSection = null;
        private readonly HubSection _answersSection = null;
        //private DataTransferManager _dataTransferManager = null;

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>


        public GroupDetailPage()
        {
            this.InitializeComponent();
            this._timerStopped = false;
            current = this;

            foreach (var section in Hub.Sections)
            {
                if (section.Name.Equals("itemGridView"))
                {
                    _questionSection = section;
                }
                else if (section.Name.Equals("answersSection"))
                {
                    _answersSection = section;
                }
            }

            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            var virtualKeyboard = Windows.UI.ViewManagement.InputPane.GetForCurrentView();
            virtualKeyboard.Showing += virtualKeyboard_Showing;
            virtualKeyboard.Hiding += virtualKeyboard_Hiding;

        }

        void virtualKeyboard_Hiding(Windows.UI.ViewManagement.InputPane sender, Windows.UI.ViewManagement.InputPaneVisibilityEventArgs args)
        {
            var trans = new TranslateTransform();
            trans.Y = 0;
            this.RenderTransform = trans;
            args.EnsuredFocusedElementInView = false;
        }

        void virtualKeyboard_Showing(Windows.UI.ViewManagement.InputPane sender, Windows.UI.ViewManagement.InputPaneVisibilityEventArgs args)
        {
            var scrollviewer = answersSection.GetFirstDescendantOfType<ScrollViewer>();
            int verticalOffset = (int)scrollviewer.VerticalOffset;
            var answerBox = _answersSection.GetFirstDescendantOfType<RichEditBox>();
            if (answerBox.Visibility == Windows.UI.Xaml.Visibility.Collapsed || Answers.Count != 0)
            {
                int offSet = (int)args.OccludedRect.Height;
                args.EnsuredFocusedElementInView = true;
                var trans = new TranslateTransform();
                trans.Y = -offSet;
                this.RenderTransform = trans;
            }
        }

        protected override void SetDisplayItems()
        {
            this.QuestionsSource.Source = Questions;
            this.QuestionCommentsSource.Source = Comments;
            this.AnswersSource.Source = Answers;
            this.questionDetailGrid.Visibility = Visibility.Visible;
            InitTimerStart(20);
        }

        private Button UpdateReadyButton
        {
            get
            {
                return _questionSection.GetDescendantsOfType<Button>().Where(x => x.Name.Equals("updateReadyButton")).First();
            }
        }

        protected override void CheckForUpdates(object sender, object e, int question_id)
        {
            try
            {
                if (!_timerStopped)
                {
                    base.CheckForUpdates(sender, e, question_id); 
                }
            }
            catch (InvalidOperationException)
            {
                // Understand how to log exceptions.
            }
            catch(Exception)
            { }
        }

        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var answerId = ((Answer)e.ClickedItem).answer_id;
            StackOverflowSource.Instance.SaveDetailInfo(Questions[0]);
            this.Frame.Navigate(typeof(AnswerDetail), answerId);
        }

        private async void UpVoteQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            bool upvoted = await UpVote();
            if (upvoted)
            {
                var upVoteButton = _questionSection.GetDescendantsOfType<AppBarButton>().Where(x => x.Name.Equals("UpVoteButton")).First();
                upVoteButton.IsEnabled = false;
            }
        }

        private async void DownVoteQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            bool downVoted = await DownVoted();
            if (downVoted)
            {
                var downVoteButton = _questionSection.GetDescendantsOfType<AppBarButton>().Where(x => x.Name.Equals("DownVoteButton")).First();
                downVoteButton.IsEnabled = false;
            }
        }

        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            //var progressRing = _questionSection.GetFirstDescendantOfType<ProgressRing>();
            //progressRing.Visibility = Visibility.Visible;
            //progressRing.IsActive = true;
            //InitTimerStop();            
        }

        private void InitTimerStop()
        {
            _timerStopped = true;
            Timer.Stop();
        }

        private void InitTimerStart(int seconds)
        {
            Timer.Interval = new TimeSpan(0, 0, seconds);
            Timer.Start();
            _timerStopped = false;
        }

        private void updateReadyButton_Click(object sender, RoutedEventArgs e)
        {
            SetSources(StackOverflowSource.Instance.DetailInfo);
            UpdateReadyButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            InitTimerStart(10);
        }

        private void ScrollHubToSection(HubSection section)
        {
            var visual = section.TransformToVisual(this.Hub);
            var point = visual.TransformPoint(new Windows.Foundation.Point(0, 0));
            var viewer = this.Hub.GetFirstDescendantOfType<ScrollViewer>();
            viewer.ChangeView(point.X, null, null);
        }

        private void AddAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                if (!Root.UserDetails.AccessToken.NullOrEmpty())
                {
                    FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility.Visible);
                    ScrollHubToSection(this._answersSection);
                }
            }
        }

        private void FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility state)
        {
            var answerBox = _answersSection.GetFirstDescendantOfType<RichEditBox>();
            foreach (var button in _answersSection.GetDescendantsOfType<AppBarButton>())
            {
                if (button.Name.Equals("submitButton") || button.Name.Equals("cancelButton"))
                {
                    button.Visibility = state;
                }
            }
            answerBox.Visibility = state;
        }

        private async void submitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string outText;
                var answerBox = _answersSection.GetFirstDescendantOfType<RichEditBox>();
                answerBox.Document.GetText(
                    Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var answer = await QueryStackOverflow.AddAnswerAsync(this.Root.UserDetails.AccessToken, QuestionId, outText);
                if (answer != null)
                {
                    FlipAnswerBoxVisibility(Windows.UI.Xaml.Visibility.Collapsed);
                    Answers.Add(answer.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].answers.Add(answer.items[0]);
                    Questions[0].answer_count = Answers.Count;
                    this.UpdateLayout();
                    var scrollViewer = _answersSection.GetFirstDescendantOfType<ScrollViewer>();
                    if (scrollViewer != null)
                    {
                        scrollViewer.ChangeView(null, scrollViewer.ScrollableHeight, null);
                    }
                }
            }
            catch (Exception )
            {

            }
        }

        private void FlipCommentBox(Windows.UI.Xaml.Visibility state)
        {
            var commentBoxControl = _questionSection.GetFirstDescendantOfType<CommentBoxControl>();
            commentBoxControl.Visibility = state;
        }

        private void addCommentButton_Click(object sender, EventArgs e)
        {
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login();
            }
            else
            {
                if (!Root.UserDetails.AccessToken.NullOrEmpty())
                {
                    FlipCommentBox(Windows.UI.Xaml.Visibility.Visible);
                    this.UpdateLayout();
                    var scrollViewer = _questionSection.GetFirstDescendantOfType<ScrollViewer>();
                    var verticalOffset = scrollViewer.VerticalOffset + 200;
                    scrollViewer.ChangeView(null, Math.Min(verticalOffset, scrollViewer.ScrollableHeight), null);
                }
            }
        }


        private void commentCancelButton_Click(object sender, EventArgs e)
        {
            FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
        }

        private async void commentsubmitButton_Click(object sender, EventArgs e)
        {
            RichEditBox commentEditBox = null;
            ProgressRing commentProgressRing = null;
            try
            {
                var commentBoxControl = _questionSection.GetFirstDescendantOfType<CommentBoxControl>();
                commentBoxControl.IsEnabled = false;
                
                commentEditBox = commentBoxControl.GetFirstDescendantOfType<RichEditBox>();

                string outText;
                commentEditBox.Document.GetText(Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out outText);
                var comment = await StackOverflowSource.AddCommentAsync(this.Root.UserDetails.AccessToken, QuestionId, outText);
                if (comment != null)
                {
                    FlipCommentBox(Windows.UI.Xaml.Visibility.Collapsed);
                    Comments.Add(comment.items[0]);
                    StackOverflowSource.Instance.DetailInfo.items[0].comments.Add(comment.items[0]);
                    Questions[0].comment_count = Comments.Count;
                    this.UpdateLayout();
                    var scrollViewer = _questionSection.GetFirstDescendantOfType<ScrollViewer>();
                    scrollViewer.ChangeView(null, scrollViewer.ScrollableHeight, null);
                } // else will be an exception handling case. We'll handle exceptions later. By putting some info on the AppBar.
            }
            catch (Exception)
            {
            }
            finally
            {
                if (commentEditBox != null)
                {
                    commentEditBox.Document.SetText(Windows.UI.Text.TextSetOptions.None, string.Empty);
                }
                if (commentProgressRing != null)
                {
                    commentProgressRing.Visibility = Visibility.Collapsed;                    
                }
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            FlipAnswerBoxVisibility(Visibility.Collapsed);
        }

        private void questionProfileImageClick(object sender, EventArgs e)
        {
            this.Frame.Navigate(typeof(UserProfile), this.Questions[0].owner.user_id.ToString());
        }

        private void answerProfileImage_Click(object sender, RoutedEventArgs e)
        {
            //var item = Utils.FindVisualParent<ItemsControl>(sender as Button);
            //this.Frame.Navigate(typeof(UserProfile), _answers[item.].owner.user_id.ToString());
        }

        private void TagButton_Click(object sender, EventArgs e)
        {
            // Here... call the root page.
            this.Frame.Navigate(typeof(GroupedItemsPage), (sender as Button).Content as string);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateUI(null, null, QuestionId); // Just call the UpdateUI.
        }

        private void Share_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }
    }
}