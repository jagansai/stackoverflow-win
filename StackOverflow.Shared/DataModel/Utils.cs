﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using System.Linq;


namespace StackOverflow
{
    public static class Utils
    {
        private static readonly DateTime UnixEpoch =
                                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        internal static DateTime ConvertFromUnix(long unixtime)
        {
            return UnixEpoch.AddSeconds(unixtime);
        }

        internal static long GetCurrentUnixTimeStampSeconds()
        {
            return (long)(DateTime.UtcNow - UnixEpoch).TotalSeconds;
        }

        internal static string Elapsed(DateTime dt)
        {
            TimeSpan ts = DateTime.UtcNow.Subtract(dt);
            return string.Format("{0:dd\\.hh\\:mm\\:ss}", ts);
        }


        internal static string GetCreationDt(DateTime dt)
        {
            TimeSpan ts = DateTime.UtcNow.Subtract(dt);
            int secs = (int)ts.TotalSeconds;
            int minutes = (int)ts.TotalMinutes;
            int hours = (int)ts.TotalHours;
            int days = (int)ts.TotalDays;
            int years = (int)(days / 365);
            int months = (int)(days / 30);
            int weeks = (int)(days / 7);

            if (years > 0)
            {
                return string.Format("{0} {1}", years, years > 1 ? "years" : "year");
            }
            else if (months > 0)
            {
                return string.Format("{0} {1}", months, months > 1 ? "months" : "months");
            }
            else if (weeks > 0)
            {
                return string.Format("{0} {1}", weeks, weeks > 1 ? "weeks" : "weeks");
            }
            else if (days > 1)
            {
                return string.Format("{0} at {1:\\.hh\\:mm}",
                    dt.ToString("m", new CultureInfo("en-US")), ts).Replace(".", string.Empty);
            }
            else if (days > 0)
            {
                return string.Format("{0} {1} ago", days, days > 1 ? "days" : "day");
            }
            else if (hours > 0)
            {
                return string.Format("{0} {1} ago", hours, hours > 1 ? "hours" : "hour");
            }
            else if (minutes > 0)
            {
                return string.Format("{0} min ago", minutes);
            }           
            else
            {
                if (secs < 30)
                {
                    return "Now";
                }
                else
                {
                    return string.Format("{0} secs ago", secs);
                }
            }
        }


        internal static string GetCreationDtShort(DateTime dt)
        {
            TimeSpan ts = DateTime.UtcNow.Subtract(dt);
            int secs = (int)ts.TotalSeconds;
            int minutes = (int)ts.TotalMinutes;
            int hours = (int)ts.TotalHours;
            int days = (int)ts.TotalDays;
            int years = (int)(days / 365);
            int months = (int)(days / 30);
            int weeks = (int)(days / 7);

            if (years > 0)
            {
                return string.Format("{0} {1}", years, years > 1 ? "years" : "year");
            }
            else if (months > 0)
            {
                return string.Format("{0} {1}", months, months > 1 ? "months" : "months");
            }
            else if (weeks > 0)
            {
                return string.Format("{0} {1}", weeks, weeks > 1 ? "weeks" : "weeks");
            }
            else if (days > 1)
            {
                return string.Format("{0} at {1:\\.hh\\:mm}",
                    dt.ToString("m", new CultureInfo("en-US")), ts).Replace(".", string.Empty);
            }
            else if (days > 0)
            {
                return string.Format("{0} {1}", days, days > 1 ? "days" : "day");
            }
            else if (hours > 0)
            {
                return string.Format("{0} {1}", hours, hours > 1 ? "hours" : "hour");
            }
            else if (minutes > 0)
            {
                return string.Format("{0} m", minutes);
            }
            else
            {
                if (secs < 30)
                {
                    return "Now";
                }
                else
                {
                    return string.Format("{0} s", secs);
                }
            }

        }


        public static void TimeTaken(Action action, string message)
        {
            Stopwatch sw = new Stopwatch();
            try
            {
                sw.Start();
                action();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sw.Stop();
                TimeSpan ts = sw.Elapsed;
            }
        }

        public static void AddRange<T>(this ObservableCollection<T> coll, IEnumerable<T> list, bool clearCollection = false)
        {            
            if (clearCollection)
            {
                coll.Clear();
            }
            foreach (var item in list)
            {
                coll.Add(item);
            }
        }


        public static int GetCount<T>(this ObservableCollection<T> t)
        {
            return t == null ? 0 : t.Count;
        }

        public static bool NullOrEmpty(this string str)
        {
            return str == null || str == string.Empty;
        }

        public static IList<T> EmptyIfNull<T>(this IList<T> t)
        {
            return t == null ? new List<T>() : t;
        }

        public static int Count<T>(this IList<T> t)
        {
            return t == null ? 0 : t.Count;
        }


        public static void ShrinkToFit<T>(this List<T> coll, int size)
        {
            int index = System.Math.Min(coll.Count, size);
            coll.RemoveRange(index, coll.Count - index);
        }


        #region VisualTreeHelpers
        public static parentItemType FindVisualParent<parentItemType>(DependencyObject o) where parentItemType : DependencyObject
        {
            while ((o != null) && !(o is parentItemType))
                return FindVisualParent<parentItemType>(VisualTreeHelper.GetParent(o));
            return o as parentItemType;
        }

        public static childItemType FindVisualChild<childItemType>(DependencyObject o) where childItemType : DependencyObject
        {
            while ((o != null) && !(o is childItemType))
            {
                return FindVisualChild<childItemType>(VisualTreeHelper.GetChild(o, 0));
            }
            return o as childItemType;
        }

        public static T GetFirstDescendantOfType<T>(this DependencyObject start) where T : DependencyObject
        {
            return start.GetDescendantsOfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Gets the descendants of the given type.
        /// </summary>
        /// <typeparam name="T">Type of descendants to return.</typeparam>
        /// <param name="start">The start.</param>
        /// <returns></returns>
        public static IEnumerable<T> GetDescendantsOfType<T>(this DependencyObject start) where T : DependencyObject
        {
            return start.GetDescendants().OfType<T>();
        }


        /// <summary>
        /// Gets the descendants.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetDescendants(this DependencyObject start)
        {
            if (start == null)
            {
                yield break;
            }

            var queue = new Queue<DependencyObject>();

            var popup = start as Popup;

            if (popup != null)
            {
                if (popup.Child != null)
                {
                    queue.Enqueue(popup.Child);
                    yield return popup.Child;
                }
            }
            else
            {
                var count = VisualTreeHelper.GetChildrenCount(start);

                for (int i = 0; i < count; i++)
                {
                    var child = VisualTreeHelper.GetChild(start, i);
                    queue.Enqueue(child);
                    yield return child;
                }
            }

            while (queue.Count > 0)
            {
                var parent = queue.Dequeue();

                popup = parent as Popup;

                if (popup != null)
                {
                    if (popup.Child != null)
                    {
                        queue.Enqueue(popup.Child);
                        yield return popup.Child;
                    }
                }
                else
                {
                    var count = VisualTreeHelper.GetChildrenCount(parent);

                    for (int i = 0; i < count; i++)
                    {
                        var child = VisualTreeHelper.GetChild(parent, i);
                        yield return child;
                        queue.Enqueue(child);
                    }
                }
            }
        }

        #endregion

   
    }
}
