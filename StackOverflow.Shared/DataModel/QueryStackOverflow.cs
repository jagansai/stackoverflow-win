﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading.Tasks;
using Windows.Web;
using System.Net.Http;

namespace StackOverflow
{
    class QueryStackOverflow
    {
        public static readonly string stackexchange = "stackexchange";
        public static readonly string site_ = "stackoverflow";
        private static readonly string version_ = "2.2";
        private static readonly string key_ = "yout_key";

        private static async Task<Type> PostToStackOverflowAsync<Type>(string url, Dictionary<string, string> values) where Type : new()
        {
            string jsonText;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;
                    var content = new FormUrlEncodedContent(values);
                    var response = await client.PostAsync(url, content);
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        using (var zipstream = new GZipStream(stream, CompressionMode.Decompress))
                        {
                            using (var reader = new StreamReader(zipstream))
                            {
                                jsonText = reader.ReadToEnd();
                            }
                        }
                    }
                }
                return JsonConvert.DeserializeObject<Type>(jsonText);
            }
            catch (Exception )
            {
                //  throw ex;
                return new Type();
            }
        }

        // Retry count.
        private static async Task<Type> ProcessStackOverFlowQueryAsync<Type>(string url) where Type : new()
        {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;
                        using (var response = await client.GetStreamAsync(url))
                        {
                            using (var zipstream = new GZipStream(response, CompressionMode.Decompress))
                            {
                                using (var reader = new StreamReader(zipstream))
                                {
                                    string jsonText = reader.ReadToEnd();
                                    return JsonConvert.DeserializeObject<Type>(jsonText);
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return new Type();
                }
            }

        public static Task<T> UpVoteAsync<T>(int id, string accessToken) where T : new()
        {
            var dict = new Dictionary<string, string>()
                {                    
                    {"preview", "false"},                    
                    {"access_token", accessToken},
                      {"site", "stackoverflow"},
                    {"key", key_}
                };
            if (typeof(T).Equals(typeof(AnswerInfo)))
            {
                string url = "https://api.stackexchange.com/{0}/answers/{1}/upvote";
                return PostToStackOverflowAsync<T>(string.Format(url, version_, id), dict);
            }
            else if (typeof(T).Equals(typeof(QuestionInfo)))
            {
                string url = "https://api.stackexchange.com/{0}/questions/{1}/upvote";
                return PostToStackOverflowAsync<T>(string.Format(url, version_, id), dict);
            }
            else
            {
                throw new Exception("Invalid type");
            }
        }

        internal static Task<SiteInfo> GetSitesAsync(int pageIndex, int pageSize = 50)
        {
            string url = "https://api.stackexchange.com/{0}/sites?page={1}&pagesize={2}";
            return ProcessStackOverFlowQueryAsync<SiteInfo>(string.Format(url, version_, pageIndex, pageSize));
        }

        public static Task<T> DownVoteAsync<T>(int id, string accessToken) where T : new()
        {

            var dict = new Dictionary<string, string>()
                {                    
                    {"preview", "false"},                    
                    {"site", "stackoverflow"},
                    {"access_token", accessToken},
                    {"key", key_}
                };

            if (typeof(T).Equals(typeof(AnswerInfo)))
            {
                string url = "https://api.stackexchange.com/{0}/answers/{1}/downvote";
                return PostToStackOverflowAsync<T>(string.Format(url, version_, id), dict);
            }
            else if (typeof(T).Equals(typeof(QuestionInfo)))
            {
                string url = "https://api.stackexchange.com/{0}/questions/{1}/downvote";
                return PostToStackOverflowAsync<T>(string.Format(url, version_, id), dict);
            }
            else
            {
                throw new Exception("Invalid type");
            }
        }


        
        public static Task<TagInfo> GetTagsAsync(string tag)
        {
            string url = "https://api.stackexchange.com/{0}/tags?order=desc&sort=popular&inname={1}&site={2}&key={3}";
            return ProcessStackOverFlowQueryAsync<TagInfo>(string.Format(url, version_, WebUtility.UrlEncode(tag), site_, key_));
        }

  
        public static Task<UserInfo> GetUsersInfoAsync(string username)
        {
            string url = "https://api.stackexchange.com/{0}/users?order=desc&sort=reputation&inname={1}&site={2}&key={3}";
            return ProcessStackOverFlowQueryAsync<UserInfo>(string.Format(url, version_, username, site_, key_));
        }

        public static Task<UserInfo> GetUserInfoAsync(string user_id)
        {
            string url = "https://api.stackexchange.com/{0}/users/{1}?order=desc&sort=reputation&site={2}&key={3}&filter={4}";
            string filter = "!b0OfN*Ru4jFk_c";
            return ProcessStackOverFlowQueryAsync<UserInfo>(string.Format(url, version_, user_id, site_, key_, filter));
        }


        public static Task<AnswerInfo> GetAnswersForUserAsync(string user_id, int pageIndex, string accessToken)
        {
            string url = "https://api.stackexchange.com/{0}/users/{1}/answers?pagesize=30&page={2}&order=desc&sort=activity&site={3}&key={4}&access_token={5}&filter={6}";
            string filter = "!*L7QmUobk.CGofDb";

            return ProcessStackOverFlowQueryAsync<AnswerInfo>(string.Format(url, version_, user_id, pageIndex, site_, key_, accessToken, filter));
        }


        public static Task<QuestionInfo> GetFavoritesForUserAsync(string user_id, int pageIndex, string accessToken)
        {
            string url = "https://api.stackexchange.com/{0}/users/{1}/favorites?order=desc&pagesize=30&page={2}&sort=activity&site={3}&filter={4}&key={5}&access_token={6}";
            string filter = "!4(L6ll8P8c.-e5r69";

            return ProcessStackOverFlowQueryAsync<QuestionInfo>(string.Format(url, version_, user_id, pageIndex, site_, filter, key_, accessToken));
        }


        public static Task<QuestionInfo> GetQuestionsForUserAsync(string user_id, int pageIndex, string accessToken)
        {

            string url = "https://api.stackexchange.com/{0}/users/{1}/questions?order=desc&sort=activity&pagesize=30&page={2}&site={3}&filter={4}&key={5}&access_token={6}";
            string filter = "!b0OfMwwD2orHkq";

            return ProcessStackOverFlowQueryAsync<QuestionInfo>(string.Format(url, version_, user_id, pageIndex, site_, filter, key_, accessToken));
        }



        public static Task<AnswerInfo> AddAnswerAsync(string access_token, int question_id, string body)
        {
            try
            {
                string url = "https://api.stackexchange.com/{0}/questions/{1}/answers/add";
                string filter = "!9YdnSM68i";

                var dict = new Dictionary<string, string>()
                {
                    { "body", body},
                    {"preview", "false"},
                    {"filter", filter},
                    {"site", "stackoverflow"},
                    {"access_token", access_token},
                    {"key", key_}
                };

                return PostToStackOverflowAsync<AnswerInfo>(string.Format(url, version_, question_id), dict);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Task<CommentInfo> AddCommentAsync(string access_token, int question_id, string body)
        {
            try
            {
                string url = "https://api.stackexchange.com/{0}/posts/{1}/comments/add";
                string filter = "!9YdnSNNBB";

                var dict = new Dictionary<string, string>()
                {
                    { "body", body},
                    {"preview", "false"},
                    {"filter", filter},
                    {"site", "stackoverflow"},
                    {"access_token", access_token},
                    {"key", key_}
                };
                return PostToStackOverflowAsync<CommentInfo>(string.Format(url, version_, question_id), dict);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static Task<UserInfo> QueryUserSitesAsync(string user_id)
        {
            const string url = "https://api.stackexchange.com/{0}/users/{1}/associated";
            return ProcessStackOverFlowQueryAsync<UserInfo>(string.Format(url, version_, user_id));
        }

        public static Task<UserInfo> QueryUserInfoAsync(string access_token, string siteName)
        {
            string url = "https://api.stackexchange.com/{0}/me?order=desc&sort=reputation&filter=default&site={1}&access_token={2}&key={3}";
                   return ProcessStackOverFlowQueryAsync<UserInfo>(string.Format(url, version_, siteName, access_token, key_));
        }

        public static Task<InboxInfo> QueryInboxAsync(int pageIndex, string accessToken)
        {
            const string filter = "!9YdnSF0HB";
            string url = "https://api.stackexchange.com/{0}/inbox/unread?page={1}&pagesize=30&access_token={2}&key={3}&filter={4}";
            return ProcessStackOverFlowQueryAsync<InboxInfo>(string.Format(url, version_, pageIndex, accessToken, key_, filter));
        }

        public static Task<InboxInfo> QueryInboxAsyncRead(int pageIndex, string accessToken)
        {
            const string filter = "!9YdnSF0HB";
            string url = "https://api.stackexchange.com/{0}/inbox?page={1}&pagesize=30&access_token={2}&key={3}&filter={4}";
            return ProcessStackOverFlowQueryAsync<InboxInfo>(string.Format(url, version_, pageIndex, accessToken, key_, filter));
        }

        public static Task<QuestionInfo> QueryTaggedQuestions(string site_name, string tag = "", int pageIndex = 1, int pageSize = 30)
        {
            if (tag == string.Empty)
            {
                return QueryQuestionsAsync(site_name, pageIndex, pageSize);
            }
            else
            {
                string url = "https://api.stackexchange.com/{0}/questions?page={1}&pagesize={2}&order=desc&sort=creation&tagged={3}&site={4}&key={5}";
                return ProcessStackOverFlowQueryAsync<QuestionInfo>(string.Format(url, version_, pageIndex, pageSize, WebUtility.UrlEncode(tag), site_name, key_));
            }
        }

        private static Task<QuestionInfo> QueryQuestionsAsync(string site_name, int pageIndex = 1, int pageSize = 30)
        {
            string url = "https://api.stackexchange.com/{0}/questions?page={1}&pagesize={2}&order=desc&sort=creation&site={3}&key={4}";
            return ProcessStackOverFlowQueryAsync<QuestionInfo>(string.Format(url, version_, pageIndex, pageSize, site_name, key_));
        }

        public static Task<QuestionInfo> QuestionDetailWithFiltersAsync(int questionId, string accessToken)
        {           
            string finalUrl;
            if (!accessToken.NullOrEmpty())
            {
                const string questionfilter = "!)bnscowzw.cX1lPxlK5L32lF3GBpuAlVP5kdORa6SCx0v";
                string url = "https://api.stackexchange.com/{0}/questions/{1}?order=desc&sort=activity&site={2}&filter={3}&key={4}&access_token={5}";
                finalUrl = string.Format(url, version_, questionId, site_, questionfilter, key_, accessToken);
            }
            else
            {
                const string questionfilter = "!)rCcH9YBVzphI2F5-v0D";
                string url = "https://api.stackexchange.com/{0}/questions/{1}?order=desc&sort=activity&site={2}&filter={3}&key={4}";
                finalUrl = string.Format(url, version_, questionId, site_, questionfilter, key_);
            }
            return ProcessStackOverFlowQueryAsync<QuestionInfo>(finalUrl);
        }



        internal static Task<AnswerInfo> AnswerDetailWithFiltersAsync(int answerId, int last_activity_date, string accessToken)
        {
            string finalUrl;
            if (!accessToken.NullOrEmpty())
            {
                const string answerfilter = "!)Qq9k9dmPbxc3)muCSgTC6cz";
                string url = "https://api.stackexchange.com/{0}/answers/{1}?order=desc&sort=activity&site={2}&filter={3}&key={4}&access_token={5}";
                finalUrl = string.Format(url, version_, answerId, site_, answerfilter, key_, accessToken);
            }
            else
            {
                const string answerfilter = "!3yXvhCa-Q(B6S.0Vm";
                string url = "https://api.stackexchange.com/{0}/answers/{1}?order=desc&sort=activity&site={2}&filter={3}&key={4}";
                finalUrl = string.Format(url, version_, answerId, site_, answerfilter, key_);
            }
            return ProcessStackOverFlowQueryAsync<AnswerInfo>(finalUrl);
        }

        public static Task<QuestionInfo> QueryQuestionWithAnswerIdAsync(int answerId)
        {

            string url = "https://api.stackexchange.com/{0}/answers/{1}?order=desc&sort=activity&site={2}&filter={3}&key={4}";
            string filter = "!--pn9sThVUTv";
            return ProcessStackOverFlowQueryAsync<QuestionInfo>(string.Format(url, version_, answerId, site_, filter, key_));
        }


    }
}
