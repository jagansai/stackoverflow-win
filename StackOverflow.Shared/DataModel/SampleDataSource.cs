﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;

namespace StackOverflow.Data
{
    [Windows.Foundation.Metadata.WebHostHidden]
    public abstract class BindableBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Multicast event for property change notifications.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

      
        protected bool SetProperty<T>(Expression<Func<T>> propertyExpression)
        {
            this.OnPropertyChanged(ExtractPropertyName(propertyExpression));
            return true;
        }

        protected bool SetProperty(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
            return true;
        }


       private static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
       {
            var memberExp = propertyExpression.Body as MemberExpression;
            if (memberExp == null)
            {
                throw new ArgumentException("Expression must be a MemberExpression.", "propertyExpression");
            }
            return memberExp.Member.Name;
       } 

      
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }    



    /*
     * Repository is only for getting new questions.
     */

    internal sealed class StackOverflowRepo
    {
        private readonly static StackOverflowRepo _repo = new StackOverflowRepo();

        public static StackOverflowRepo Instance
        {
            get
            {
                return _repo;
            }
        }

        public string Tags
        {
            set
            {
                _tags = value;
            }
        }

        internal void SetInfo(string tags, Question question)
        {
            DoWriteOperation(() =>
                {
                    _tags = tags;
                    _referenceQuestion = question;
                });
        }


        private StackOverflowRepo()
        {
            _rwSlim = new ReaderWriterLockSlim();
            _lookForQuestionsTask = Task.Factory.StartNew(async () =>
            {
                await KeepQueryingQuestionsAsyncImpl();
            }).Unwrap();
        }


        private bool CanQueryForNewQuestions()
        {
            return DoReadOperation(() =>
                {
                    return _newQuestions == null                     ? true : 
                        _newQuestions.items.Count < _maxNumQuestions ? true :
                        ++_retryCount > _maxRetryCount;
                });
        }

        private void ResetRetryCount()
        {
            if (_retryCount >= _maxRetryCount)
            {
                _retryCount = 0;
                DoWriteOperation(() =>
                {
                    _newQuestions.items.Clear();
                });
            }
        }

        private async Task KeepQueryingQuestionsAsyncImpl()
        {
            while (true)
            {
                await Task.Delay(20 * 1000); // delay of 20 seconds.
                if (CanQueryForNewQuestions())
                {
                    ResetRetryCount();
                    var info = await QueryStackOverflow.QueryTaggedQuestions(QueryStackOverflow.site_);
                    // Now check for the existing question in the new question.
                    // if it's not found then, add all these questions to the list.
                    // if it's found, then get only a range of questions that are not part of the existing list.
                    DoWriteOperation(() =>
                        {

                            if (_newQuestions == null)
                            {
                                _newQuestions = new QuestionInfo();
                                _newQuestions.items = new List<Question>(info.items);
                            }
                            else
                            {
                                _newQuestions.items.AddRange(info.items);
                            }
                           
                            _newQuestions.items.Sort((x, y) =>
                            {
                                if (y.creation_date == x.creation_date)
                                    return 0;
                                if (y.creation_date < x.creation_date)
                                    return -1;
                                return 1;
                            });


                            //// Take only those questions that are not part of the view.
                            //var questionsToAdd = info.items.FindAll((item) =>
                            //{
                            //    //return item.question_id > _referenceQuestionId &&
                            //    //       (_tags == string.Empty || item.tags.Contains(_tags));

                            //    return item.question_id > _referenceQuestion.question_id;
                            //});

                            //if (questionsToAdd.Count > 0)
                            //{
                            //    if (_newQuestions == null)
                            //    {
                            //        _newQuestions = new QuestionInfo();
                            //        _newQuestions.items = new List<Question>(questionsToAdd);
                            //    }
                            //    else
                            //    {
                            //        // Find the questions that are new ( i.e. the question id greater than the existing questions
                            //        // and whose tags match the tag that is required.
                            //        // If we are not looking for any specific tag, then return true.

                            //        if (_newQuestions.items.Count > 0)
                            //        {
                            //            // Just in case there are questions from other tags.
                            //            //if (_tags != string.Empty)
                            //            //{
                            //            //    _newQuestions.items.RemoveAll(item => !item.tags.Contains(_tags));
                            //            //}
                                      
                            //          //  _newQuestions.UpdateAvailable = true;
                            //        } // if the newQuestions is empty.
                            //        else
                            //        {
                            //            // Get only those questions whose tags match the tag that is required.
                            //            // If we are not interested any specific tags, then return all the questions.
                            //            _newQuestions.items.AddRange(questionsToAdd);
                            //            // _newQuestions.UpdateAvailable = true;
                            //        }
                            //    }
                            //}
                            //_tagRepo.AddRange(_newQuestions.items.Select(x => x.tags));
                        });
                }
            }
        }

        private void DoReadOperation(Action action)
        {
            try
            {
                _rwSlim.EnterReadLock();
                action();
            }
            catch (Exception )
            {
            }
            finally
            {
                _rwSlim.ExitReadLock();
            }
        }

        private Ret DoReadOperation<Ret>(Func<Ret> func)
        {
            try
            {
                _rwSlim.EnterReadLock();
                return func();
            }
            catch (Exception )
            {
                throw;
            }
            finally
            {
                _rwSlim.ExitReadLock();
            }
        }

        private void DoWriteOperation(Action action)
        {
            try
            {
                _rwSlim.EnterWriteLock();
                action();
            }
            catch (Exception )
            {
                throw;
            }
            finally
            {
                _rwSlim.ExitWriteLock();
            }
        }

        private Ret DoWriteOperation<Ret>(Func<Ret> func)
        {
            try
            {
                _rwSlim.EnterWriteLock();
                return func();
            }
            catch (Exception )
            {
                throw;
            }
            finally
            {
                _rwSlim.ExitWriteLock();
            }
        }

        #region data memebers

        private QuestionInfo _newQuestions = null;
        private ReaderWriterLockSlim _rwSlim = null;
        private Task _lookForQuestionsTask = null;
        private string _tags = "";
        private int _maxNumQuestions = 60; // keep only these many questions in the master list.
        private readonly int _maxRetryCount = 2;
        // This is to make sure after maxRetryCount, clear the items. So that we get a chance to query for new questions.
        private int _retryCount = 0;
        private Question _referenceQuestion; // this is the latest Question Id from StackOverflowInstance.
        private TagInfo _tagRepo = new TagInfo();
        

        #endregion


        #region public members

        internal void KeepQueryingQuestions()
        {
          
        }

        internal bool NewQuestionsArrived(long latest_creation_date, string tags)
        {
            return DoReadOperation(() =>
                {
                    // return _newQuestions == null ? false : _newQuestions.UpdateAvailable;

                    if (_newQuestions == null)
                    {
                        return false;
                    }
                    /// Now check if there are questions that are later than the current ones.
                    /// 
                    if (tags.Equals(string.Empty))
                    {
                        return _newQuestions.items.Any((item) => item.creation_date > latest_creation_date);
                    }
                    else
                    {
                        return _newQuestions.items.Any((item) => (item.creation_date > latest_creation_date) &&
                                                                  (item.tags.Contains(tags)));
                    }                    
                });
        }

        internal List<Question> TakeNewQuestions(string tag, long latest_creation_date)
        {
            return DoWriteOperation(() =>
                {
                    List<Question> items = new List<Question>(_newQuestions.items.Count);

                    foreach(var item in _newQuestions.items)
                    {
                        if (item.creation_date > latest_creation_date)
                        {
                            if (tag.Equals(string.Empty))
                            {
                                items.Add(item);
                                item.Consumed = true;
                            }
                            else if (item.tags.Contains(tag))
                            {
                                items.Add(item);
                                item.Consumed = true;
                            }
                        }
                    }
                    _newQuestions.items.RemoveAll(item => item.Consumed == true);
                    return items;
                });
        }

        internal async Task<QuestionInfo> LoadNewQuestionsOnDemand(string tag, int pageIndex)
        {
            QuestionInfo info = await QueryStackOverflow.QueryTaggedQuestions(QueryStackOverflow.site_, tag, pageIndex);
            return info;
        }


        #endregion        
    }


    public sealed class StackOverflowSource
    {
        private readonly static StackOverflowSource _stackOverflowSource = new StackOverflowSource();
        private QuestionInfo _questionInfo = new QuestionInfo();
        private QuestionInfo _questionDetailUpdateInfo = new QuestionInfo();
        private AnswerInfo _answerUpdateInfo = new AnswerInfo();
        private InboxInfo _inboxInfo = new InboxInfo();

        private string _tags;

        public string Tags
        {
            get
            {
                return _tags;
            }
            private set
            {
                _tags = value;
            }
        }

        public void SetData(QuestionInfo questionInfo)
        {
            _questionDetailUpdateInfo = questionInfo;
        }

        private StackOverflowSource()
        {
        }

        public static StackOverflowSource Instance
        {
            get
            {
                return _stackOverflowSource;
            }
        }

        public QuestionInfo Info
        {
            get
            {
                return _questionInfo;
            }
            private set
            {
                _questionInfo = value;
            }
        }

        public Task<TagInfo> GetTags(string tag)
        {
            return QueryStackOverflow.GetTagsAsync(tag);
        }

        public Task<UserInfo> GetUsers(string username)
        {
            return QueryStackOverflow.GetUsersInfoAsync(username);
        }



        public QuestionInfo DetailInfo
        {
            get
            {
                return _questionDetailUpdateInfo;
            }
            set
            {
                _questionDetailUpdateInfo = value;
            }
        }

        public AnswerInfo AnswerDetailInfo
        {
            get
            {
                return _answerUpdateInfo;
            }
        }


        public static  Task<T> SetUpVote<T>(int id, string accessToken) where T : new()
        {
            return QueryStackOverflow.UpVoteAsync<T>(id, accessToken);
        }


        public static Task<T> SetDownVote<T>(int id, string accessToken) where T : new()
        {
            return QueryStackOverflow.DownVoteAsync<T>(id, accessToken);
        }
        


        public static async Task<bool> SetVoteForAnswer(int questionId)
        {
            return await Task.Factory.StartNew<bool>(() => { Task.Delay(1000); return true; });
        }



        public async Task<QuestionInfo> GetQuestionsAsync(string tags = "", int pageIndex = 1)
        {
            var info = await QueryStackOverflow.QueryTaggedQuestions(QueryStackOverflow.site_,  tags, pageIndex);
            StackOverflowRepo.Instance.SetInfo(tags, info.items[0]);
            StackOverflowSource.Instance.Tags = tags;
            return info;
        }

        public void SaveInfoData(System.Collections.ObjectModel.ObservableCollection<Question> items)
        {
            if (_questionInfo == null)
            {
                _questionInfo = new QuestionInfo();
            }
            _questionInfo.items.Clear();
            _questionInfo.items.AddRange(items);
        }

        public void SaveDetailInfo(Question question)
        {
            _questionDetailUpdateInfo.items.Clear();
            _questionDetailUpdateInfo.items.Add(question);
        }

        public bool NewQuestionsArrived(string tag, long latest_creation_date)
        {
            return StackOverflowRepo.Instance.NewQuestionsArrived(latest_creation_date, tag);
        }

        public List<Question> LoadNewQuestions(string tag, long latest_creation_date)
        {
            return StackOverflowRepo.Instance.TakeNewQuestions(tag, latest_creation_date);
        }

        public Task<QuestionInfo> GetQuestionDetailsAsync(int id, string access_token)
        {
            return QueryStackOverflow.QuestionDetailWithFiltersAsync(id, access_token);
        }

        internal async Task<bool> CheckForUpdate(int questionId, int last_activity_date, string access_token)
        {
            _questionDetailUpdateInfo = await QueryStackOverflow.QuestionDetailWithFiltersAsync(questionId, access_token);
            return _questionDetailUpdateInfo.items[0].CheckForUpdate(last_activity_date);
        }

        internal async Task<bool> CheckForAnswerUpdate(int answerId, int last_activity_date, string access_token)
        {
            _answerUpdateInfo = await QueryStackOverflow.AnswerDetailWithFiltersAsync(answerId, last_activity_date, access_token);
            return _answerUpdateInfo.items[0].CheckForUpdate(last_activity_date);
        }

        public Task<QuestionInfo> LoadQuestionAsync(int questionId, string access_token)
        {
            return QueryStackOverflow.QuestionDetailWithFiltersAsync(questionId, access_token);
        }


        internal async Task<bool> CheckForNewInboxItems(int pageIndex, string access_token)
        {
            _inboxInfo = await QueryStackOverflow.QueryInboxAsync(pageIndex, access_token);
            return _inboxInfo.items == null ? false : _inboxInfo.items.Any(x => x.is_unread);
        }

        internal Task<QuestionInfo> LoadQuestionsOnDemand(string tag, int pageIndex)
        {
            return StackOverflowRepo.Instance.LoadNewQuestionsOnDemand(tag, pageIndex);
        }

        internal async Task<InboxInfo> LoadInboxItems(int pageIndex, string access_token)
        {
            if (_inboxInfo.items == null || _inboxInfo.items.Count == 0)
            {
                _inboxInfo = await QueryStackOverflow.QueryInboxAsync(pageIndex, access_token);
                var readitems = await QueryStackOverflow.QueryInboxAsyncRead(pageIndex, access_token);
                _inboxInfo.items.AddRange(readitems.items);
                _inboxInfo.items.ShrinkToFit(30);
            }
            return _inboxInfo;
        }


        public static async Task<IEnumerable<Answer>> GetAnswerDetailsAsync(int answerId)
        {
            //var info = await QueryStackOverflow.QuestionWithFiltersAsync(answerId);
            // Call AnswerWithDetails with a different filter.
            // Implementation is pending.
            return await Task.Factory.StartNew<IEnumerable<Answer>>(() =>
            {
                Task.Delay(1000);
                // ObservableCollection<Answer> answer = new ObservableCollection<Answer>();
                return _stackOverflowSource.DetailInfo.items[0].answers.Where(a => a.answer_id == answerId);
                //foreach(var ans in _stackOverflowSource.Groups[0].Answers.Where(a => a.answer_id == answerId))
                //{
                //    answer.Add(ans);
                //}
                //return answer;
            });
        }

        public static Task<CommentInfo> AddCommentAsync(string accessToken, int questionId, string text)
        {
            return QueryStackOverflow.AddCommentAsync(accessToken, questionId, text);
        }



        #region UnitTestingPurpose
        private static async Task<QuestionInfo> GetQuestionsAsyncImpl(string uri)
        {
            Uri dataUri = new Uri(uri);
            var file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            return JsonConvert.DeserializeObject<QuestionInfo>(jsonText);
        }

        #endregion




     
    }


}