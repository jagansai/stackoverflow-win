﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace StackOverflow
{
    public sealed partial class TextOverImage : UserControl
    {
        public TextOverImage()
        {
            this.InitializeComponent();
        }

        public static readonly DependencyProperty ImageTextProperty =
DependencyProperty.Register("ImageText", typeof(String),
typeof(TextOverImage), new PropertyMetadata(string.Empty));

        public String ImageText
        {
            get { return GetValue(ImageTextProperty).ToString(); }
            set { SetValue(ImageTextProperty, value); }
        }

    }
}
