﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace StackOverflow
{
    public sealed partial class AddCommentAndUserControl : UserControl
    {
        public AddCommentAndUserControl()
        {
            this.InitializeComponent();
        }

        public event EventHandler addCommentClick;
        public event EventHandler profileImageClick;

        private void addCommentButton_Click(object sender, RoutedEventArgs e)
        {
            this.addCommentClick(sender, EventArgs.Empty);
        }


        private void profileImage_Click(object sender, RoutedEventArgs e)
        {
            this.profileImageClick(sender, EventArgs.Empty);
        }
    }
}
