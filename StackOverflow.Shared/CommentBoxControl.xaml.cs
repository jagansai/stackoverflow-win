﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace StackOverflow
{
    public sealed partial class CommentBoxControl : UserControl
    {
        public event EventHandler commentSubmitClick;
        public event EventHandler commentCancelClick;
        public CommentBoxControl()
        {
            this.InitializeComponent();
        }

        private void CommentSubmit_Click(object sender, RoutedEventArgs e)
        {
            commentSubmitProgress.Visibility = Visibility.Visible;
            commentSubmitProgress.IsActive = true;
            this.UpdateLayout();
            this.commentSubmitClick(sender, EventArgs.Empty);
            commentSubmitProgress.Visibility = Visibility.Collapsed;
            commentSubmitProgress.IsActive = false;
        }

        private void commentBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            string text;
            this.commentBox.Document.GetText(Windows.UI.Text.TextGetOptions.IncludeNumbering | Windows.UI.Text.TextGetOptions.UseObjectText, out text);
            this.commentsubmitButton.IsEnabled = text.Length > 15 && text.Length <= 600;
            const int minThreshold = 300;
            int textLength = text.Length;
            if (textLength == 0)
            {
                this.remaniningChars.Text = "enter at least 15 characters";
            }
            else if (textLength > 0 && text.Length < 15)
            {
                this.remaniningChars.Text = string.Format("{0} more to go...", 15 - textLength);
            }
            else
            {
                this.remaniningChars.Text = string.Format("{0} characters left", 600 - textLength);
               
            }
            if (600 - textLength < minThreshold)
            {
                this.remaniningChars.Foreground = new SolidColorBrush(Windows.UI.Colors.Red);
            }
            //else
            //{
            //    this.remaniningChars.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
            //}
        }

        private void CancelComment_Click(object sender, RoutedEventArgs e)
        {
            this.commentCancelClick(sender, EventArgs.Empty);
        }
    }
}
