﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Markup;

namespace StackOverflow.Common
{
    /// <summary>
    /// Usage: 
    /// 1) In a XAML file, declare the above namespace, e.g.:
    ///    xmlns:common="using:StackOverflow.Common"
    ///     
    /// 2) In RichTextBlock controls, set or databind the Html property, e.g.:
    /// 
    ///    <RichTextBlock common:Properties.Html="{Binding ...}"/>
    ///    
    ///    or
    ///    
    ///    <RichTextBlock>
    ///       <common:Properties.Html>
    ///         <![CDATA[
    ///             <p>This is a list:</p>
    ///             <ul>
    ///                 <li>Item 1</li>
    ///                 <li>Item 2</li>
    ///                 <li>Item 3</li>
    ///             </ul>
    ///         ]]>
    ///       </common:Properties.Html>
    ///    </RichTextBlock>
    /// </summary>


    public class MyHtml2XamlConverter : Html2XamlConverter
    {
        public MyHtml2XamlConverter()
            : base()
        {
            tags.Add("pre", new TagDefinition(parsePre) { MustBeTop = true });
        }

        private void parsePre(StringBuilder xamlString, HtmlNode node, bool isTop)
        {            
            xamlString.Append("<Span FontFamily=\"Consolas\" FontSize=\"21\">");
            xamlString.Append(node.InnerText.Replace("\n", "<LineBreak/>").Replace(" ", "<Run Text=\" \"/>"));
            xamlString.Append("</Span>");   
        }
    }

    public class Properties : DependencyObject
    {
        public static readonly DependencyProperty ConverterProperty =
                DependencyProperty.RegisterAttached("Converter", typeof(MyHtml2XamlConverter), typeof(Properties), new PropertyMetadata(new MyHtml2XamlConverter()));

        public static Html2XamlConverter GetConverter(DependencyObject obj)
        {
            return (Html2XamlConverter)obj.GetValue(ConverterProperty);
        }

        public static void SetConverter(DependencyObject obj, Html2XamlConverter value)
        {
            obj.SetValue(ConverterProperty, value);
        }
        public static readonly DependencyProperty HtmlProperty =
            DependencyProperty.RegisterAttached("Html", typeof(string), typeof(Properties), new PropertyMetadata(null, HtmlChanged));

        public static void SetHtml(DependencyObject obj, string value)
        {
            obj.SetValue(HtmlProperty, value);
        }

        public static string GetHtml(DependencyObject obj)
        {
            return (string)obj.GetValue(HtmlProperty);
        }

        public static Dictionary<string, Dictionary<string, string>> TagAttributes = null;
        private static void HtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RichTextBlock)
            {
                string html = e.NewValue as string;

                // Get the target RichTextBlock
                RichTextBlock richText = d as RichTextBlock;
                if (richText == null) return;
                RichTextBlock newRichText = null;
                try
                {
                    richText.Blocks.Clear();
                    // Wrap the value of the Html property in a div and convert it to a new RichTextBlock
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<?xml version=\"1.0\"?>");
                    sb.AppendLine("<RichTextBlock xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\">");
                    sb.AppendLine(GetConverter(d).Convert2Xaml(html, TagAttributes));
                    sb.AppendLine("</RichTextBlock>");
                    newRichText = (RichTextBlock)XamlReader.Load(sb.ToString());
                }
                catch (Exception)
                {
                    newRichText = new RichTextBlock();
                }
                // Move the blocks in the new RichTextBlock to the target RichTextBlock
                for (int i = newRichText.Blocks.Count - 1; i >= 0; i--)
                {
                    Block b = newRichText.Blocks[i];
                    newRichText.Blocks.RemoveAt(i);
                    richText.Blocks.Insert(0, b);
                } 
            }

        }

        private static string EncodeXml(string xml)
        {
            try
            {
                string encodedXml = xml.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                return encodedXml;
            }
            catch(Exception )
            {
                return string.Empty;
            }
        }
    }
}
