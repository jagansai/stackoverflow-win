﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace StackOverflow.Common
{
    public abstract class SharePage : Page
    {
        private DataTransferManager _dataTransferManager = null;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this._dataTransferManager = DataTransferManager.GetForCurrentView();
            this._dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
          
            this._dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            GetShareContent(args.Request);
        }

        protected abstract bool GetShareContent(DataRequest request);
       
    }
}
