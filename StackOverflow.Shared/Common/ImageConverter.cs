﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace StackOverflow.Common
{
    public sealed class CommentsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int count = System.Convert.ToInt32(value);
            return count == 1 ? string.Format("{0} Comment", count) : string.Format("{0} Comments", count);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }


    public sealed class AnswersConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int count = System.Convert.ToInt32(value);
            return count == 1 ? string.Format("{0} Answer", count) : string.Format("{0} Answers", count);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class AcceptConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var accepted = System.Convert.ToBoolean(value);
            return accepted ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class AcceptedAnswerConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var accepted_answer_id = System.Convert.ToInt32(value);
            return accepted_answer_id != 0 ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class VoteConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var votes = System.Convert.ToInt32(value);
            return string.Format("{0} {1}", votes, (votes == 1 ? "Vote" : "Votes"));
            // return votes == 1 ? "Vote" : "Votes";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class UpdateArrived : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = System.Convert.ToBoolean(value);
            return val ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }


    public sealed class ColorChangeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = System.Convert.ToBoolean(value);
            return val ? Application.Current.Resources["AppBarItemDisabledForegroundThemeBrush"] as SolidColorBrush :
                         Application.Current.Resources["AppBarItemForegroundThemeBrush"] as SolidColorBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class InboxColorChangeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = System.Convert.ToBoolean(value);
            return val ? Application.Current.Resources["AppBarItemDisabledForegroundThemeBrush"] as SolidColorBrush :
                         Application.Current.Resources["AppBarItemBackgroundThemeBrush"] as SolidColorBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }


    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// If set to True, conversion is reversed: True will become Collapsed.
        /// </summary>
        public bool IsReversed { get; set; }

        public BooleanToVisibilityConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = System.Convert.ToBoolean(value);
            if (this.IsReversed)
            {
                val = !val;
            }

            if (val)
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
