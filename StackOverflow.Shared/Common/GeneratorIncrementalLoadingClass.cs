﻿using StackOverflow.Data;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace StackOverflow.Common
{

    public class IncrementalLoadingQuestionClass : ObservableCollection<Question>, ISupportIncrementalLoading, INotifyCollectionChanged
    {

        public IncrementalLoadingQuestionClass(int maxPageCount, string userId)
        {
            _maxPages = maxPageCount;
            _userId = userId;
        }


        public bool DoneLoading
        {
            get
            {
                return _doneLoading;
            }
        }


        #region IncrementalLoading
        public bool HasMoreItems
        {
            get
            {
                return _pageIndex < _maxPages;
            }
        }

        public Windows.Foundation.IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            CoreDispatcher dispatcher = Window.Current.Dispatcher;

            return Task.Run<LoadMoreItemsResult>(
                async () =>
                {
                    _doneLoading = false;
                    ++_pageIndex;
                    var info = await QueryStackOverflow.GetQuestionsForUserAsync(_userId, _pageIndex, GroupedItemsPage.current.UserDetails.AccessToken);
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        for (int i = 0; info.items != null && i < info.items.Count; ++i)
                        {
                            if (i + 1 == info.items.Count)
                            {
                                _doneLoading = true;
                            }
                            this.Add(info.items[i]);
                        }
                    });
                    return new LoadMoreItemsResult() { Count = (uint)this.Count };
                }).AsAsyncOperation<LoadMoreItemsResult>();
        }

        #endregion

        private int _maxPages;
        private string _userId;

        #region State
        private int _pageIndex = 0;
        private bool _doneLoading = false;
        #endregion
    }

    public class IncrementalLoadingFavClass : ObservableCollection<Question>, ISupportIncrementalLoading, INotifyCollectionChanged
    {

        public IncrementalLoadingFavClass(int maxPageCount, string userId)
        {
            _maxPages = maxPageCount;
            _userId = userId;
        }


        public bool DoneLoading
        {
            get
            {
                return _doneLoading;
            }
        }


        #region IncrementalLoading
        public bool HasMoreItems
        {
            get
            {
                return _pageIndex < _maxPages;
            }
        }

        public Windows.Foundation.IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            CoreDispatcher dispatcher = Window.Current.Dispatcher;

            return Task.Run<LoadMoreItemsResult>(
                async () =>
                {
                    _doneLoading = false;
                    ++_pageIndex;
                    var info = await QueryStackOverflow.GetFavoritesForUserAsync(_userId, _pageIndex, GroupedItemsPage.current.UserDetails.AccessToken);
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        for (int i = 0; info.items != null && i < info.items.Count; ++i)
                        {
                            if (i + 1 == info.items.Count)
                            {
                                _doneLoading = true;
                            }
                            this.Add(info.items[i]);
                        }
                    });
                    return new LoadMoreItemsResult() { Count = (uint)this.Count };
                }).AsAsyncOperation<LoadMoreItemsResult>();
        }

        #endregion

        private int _maxPages;
        private string _userId;

        #region State
        private int _pageIndex = 0;
        private bool _doneLoading = false;
        #endregion
    }



    public class IncrementalLoadingAnswerClass : ObservableCollection<Answer>, ISupportIncrementalLoading, INotifyCollectionChanged
    {

        public IncrementalLoadingAnswerClass(int maxPageCount, string userId)
        {
            _maxPages = maxPageCount;
            _userId = userId;
        }


        public bool DoneLoading
        {
            get
            {
                return _doneLoading;
            }
        }

        public bool LoadMore
        {
            set
            {
                _loadMore = value;
            }
        }


        #region IncrementalLoading
        public bool HasMoreItems
        {
            get
            {
                return _pageIndex < _maxPages;
            }
        }

        public Windows.Foundation.IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            CoreDispatcher dispatcher = Window.Current.Dispatcher;

            return Task.Run<LoadMoreItemsResult>(
                async () =>
                {
                    if (_loadMore)
                    {
                        ++_pageIndex;
                        var info = await QueryStackOverflow.GetAnswersForUserAsync(_userId, _pageIndex, GroupedItemsPage.current.UserDetails.AccessToken);
                        await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            for (int i = 0; info.items != null && i < info.items.Count; ++i)
                            {
                                if (i + 1 == info.items.Count)
                                {
                                    _doneLoading = true;
                                }
                                this.Add(info.items[i]);
                            }
                            _doneLoading = true;
                        });
                    }
                    return new LoadMoreItemsResult() { Count = (uint)this.Count };
                }).AsAsyncOperation<LoadMoreItemsResult>();
        }

        #endregion

        private int _maxPages;
        private string _userId;

        #region State
        private int _pageIndex = 0;
        private bool _doneLoading = false;
        private bool _loadMore = true;
        #endregion
    }




    // This class implements IncrementalLoadingBase. 
    public class GeneratorIncrementalLoadingClass : ObservableCollection<Question>, INotifyCollectionChanged
    {

        public GeneratorIncrementalLoadingClass(int maxPageCount, string tags)
        {
            Initialize(maxPageCount, tags, false);

        }

        private void Initialize(int maxPageCount, string tags, bool backButtonPressed)
        {
            _maxPageCount = maxPageCount;
            _tags = tags;
            _doneLoading = false;
            _chunkSize = 30; // for now.
            _backButton = backButtonPressed;
        }

        public GeneratorIncrementalLoadingClass(int maxPageCount, bool backButtonPressed = false, string tags = "")
        {

            Initialize(maxPageCount, tags, backButtonPressed);
            if (!backButtonPressed)
            {
                if (StackOverflowSource.Instance.Info != null)
                {
                    StackOverflowSource.Instance.Info.Clear();
                }
            }
        }

        public bool DoneLoading
        {
            get
            {
                return _doneLoading;
            }
        }

        public bool BackButton
        {
            get
            {
                return _backButton;
            }
            set
            {
                _backButton = value;
            }
        }

        public string Tags
        {
            get
            {
                return _tags;
            }
            set
            {
                _tags = value;
            }
        }

        public void Reset(string tags)
        {
            _tags = tags;
            _pageIndex = 0;
            this.ClearItems();
            StackOverflowSource.Instance.Info.Clear();
        }


        #region State

        int _pageIndex = 0;
        int _maxPageCount;
        string _tags;
        bool _doneLoading;
        int _chunkSize;
        bool _backButton;
        bool _scrollMore = true;
        #endregion


        public bool ScrollMore
        {
            get
            {
                return _scrollMore;
            }
            set
            {
                _scrollMore = value;
            }
        }


        public void RemoveRange()
        {
            // Don't Set scrollbar.
            _doneLoading = false;
            if (this.Count > _chunkSize)
            {
                int count = this.Count - _chunkSize;
                // Reset the page index accordingly. Remove the items from the StackOverflowSource as well.
                _pageIndex -= (count / _chunkSize);
                for (; count-- > 0; )
                {
                    this.RemoveAt(_chunkSize);
                }
            }
            StackOverflowSource.Instance.Info.items.Clear();
            _doneLoading = true;
        }

        #region ISupportIncrementalLoading

        public bool HasMoreItems
        {
           get
            {
                return _pageIndex < _maxPageCount && ScrollMore;
            }
        }

        public Windows.Foundation.IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync()
        {
            CoreDispatcher dispatcher = Window.Current.Dispatcher;

            return Task.Run<LoadMoreItemsResult>(
                async () =>
                {
                    _doneLoading = false;
                    ++_pageIndex;

                    if (StackOverflowSource.Instance.Info.items.Count != 0)
                    {
                        var info = new QuestionInfo(StackOverflowSource.Instance.Info);
                        await AddItemsToCollection(dispatcher, info);
                        _pageIndex = (StackOverflowSource.Instance.Info.items.Count / _chunkSize);
                        StackOverflowSource.Instance.Info.items.Clear();
                    }
                    //else if (NewQuestionsArrived())
                    //{
                    //    await LoadNewQuestionsOnDemandAsync();
                    //}
                    else
                    {
                        var info = await StackOverflowSource.Instance.LoadQuestionsOnDemand(_tags, _pageIndex);
                        await AddItemsToCollection(dispatcher, info);
                    } 
                    ScrollMore = (CountMoreThanAllowedRange() == false);

                    return new LoadMoreItemsResult() { Count = (uint)this.Count };
                }).AsAsyncOperation();
        }

        private async Task AddItemsToCollection(CoreDispatcher dispatcher, QuestionInfo info)
        {
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                for (int i = 0; i < info.items.Count; ++i)
                {
                    if (i == info.items.Count - 1)
                    {
                        _doneLoading = true;
                    }
                    this.Add(info.items[i]);
                }
            });
        }

        private bool CountMoreThanAllowedRange()
        {
            return this.Count >= (_chunkSize * 3);
        }

        private bool CountEqualToAllowedRange()
        {
            return this.Count == (_chunkSize * 3);
        }

        #endregion


        public bool NewQuestionsArrived()
        {
            return StackOverflowSource.Instance.NewQuestionsArrived(_tags, this.Count == 0 ? 0 : this[0].creation_date);
        }

        public async Task LoadNewQuestionsOnDemandAsync()
        {
            _pageIndex = 0;
            var info = await StackOverflowSource.Instance.LoadQuestionsOnDemand(_tags, _pageIndex);
            var dispatcher = Window.Current.Dispatcher;
            await AddItemsToCollection(dispatcher, info); 
        }

        public async Task LoadNewQuestionsAsync()
        {
            var newQuestions = StackOverflowSource.Instance.LoadNewQuestions(_tags, this.Count == 0 ? 0 : this[0].creation_date).Except(this, ProjectionEqualityComparer<Question>.Create( x => x.question_id));
            
            CoreDispatcher dispatcher = Window.Current.Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {                                                       
                for (int i = 0; i < this.Count; ++i)
                {
                    this[i].creation_date = Utils.GetCurrentUnixTimeStampSeconds();
                }

                _doneLoading = false;
                foreach (var item in newQuestions.Reverse())
                {
                    this.InsertItem(0, item);
                }
                _doneLoading = true;
                // set the page index accordingly.
                _pageIndex = (this.Count / _chunkSize);
            });
        }
    }
}
