﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Background;

namespace StackOverflow.Common
{
    public class BackgroundTask
    {

        public const string SampleBackgroundTaskEntryPoint = "SOQuestionNotifier.MonitorQuesAndInbox";
        public const string SampleBackgroundTaskName = "MonitorQuestionsTask";
        public static string SampleBackgroundTaskProgress = string.Empty;
        public static bool SampleBackgroundTaskRegistered = false;



        public static BackgroundTaskRegistration RegisterBackgroundTask(String taskEntryPoint, String name, IBackgroundTrigger trigger, IBackgroundCondition condition)
        {
            var builder = new BackgroundTaskBuilder();

            builder.Name = name;
            builder.TaskEntryPoint = taskEntryPoint;

            if (condition != null)
            {
                builder.AddCondition(condition);
            }
            builder.SetTrigger(trigger);
            BackgroundTaskRegistration task = builder.Register();
            return task;
        }

        public static void UnregisterBackgroundTasks(string name)
        {
            //
            // Loop through all background tasks and unregister any with SampleBackgroundTaskName or
            // SampleBackgroundTaskWithConditionName.
            //
            foreach (var cur in BackgroundTaskRegistration.AllTasks)
            {
                if (cur.Value.Name == name)
                {
                    cur.Value.Unregister(true);
                }
            }            
        }
    }
}
