﻿using StackOverflow.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Navigation;

namespace StackOverflow.Views
{
    public enum ScrollPositions
    {
        ScrollToPrevious,
        ScrollToBegin,
        ScrollToEnd,
        ScrollNormal // Just stay there. No jumping here and there :-)
    }

    public abstract class MainPageBase : Page
    {
        private readonly NavigationHelper _navigationHelper;
        private GeneratorIncrementalLoadingClass _questions = null;
        private static ObservableCollection<User> _communites = new ObservableCollection<User>();
        private static UserInfo _userInfo = new UserInfo();
        private static Dictionary<string, string> _sites = null; // siten_name is key, api_site_parameter is value.
        private readonly int _maxPages = 30;
        private ScrollPositions _scrollPositions = ScrollPositions.ScrollNormal;
        private ScrollViewer _scrollViewer = null;
        private static double _scrollViewerHeight = 0.0; // maintain the previous height.  
        private readonly DispatcherTimer _timer;

        public ScrollViewer ScrollViewer
        {
            get
            {
                return _scrollViewer;
            }

            set
            {
                _scrollViewer = value;
            }
        }

        protected ScrollPositions ScrollPosition
        {
            get
            {
                return _scrollPositions;
            }

            set
            {
                _scrollPositions = value;
            }
        }

        public GeneratorIncrementalLoadingClass Questions
        {
            get
            {
                return _questions;
            }

            set
            {
                _questions = value;
            }
        }

        public static double ScrollViewerHeight
        {
            get
            {
                return _scrollViewerHeight;
            }

            set
            {
                _scrollViewerHeight = value;
            }
        }

        public NavigationHelper NavigationHelper
        {
            get
            {
                return _navigationHelper;
            }
        }

        public DispatcherTimer Timer
        {
            get
            {
                return _timer;
            }
        }

        public static ObservableCollection<User> Communites
        {
            get
            {
                return _communites;
            }

            set
            {
                _communites = value;
            }
        }

        public static UserInfo UserInfoDetails
        {
            get
            {
                return _userInfo;
            }

            set
            {
                _userInfo = value;
            }
        }

        public static Dictionary<string, string> Sites
        {
            get
            {
                return _sites;
            }
            set
            {
                _sites = value;
            }
        }

        public abstract void SetDisplayItems(string param);

        public MainPageBase()
        {
            _navigationHelper = new NavigationHelper(this);
            _navigationHelper.LoadState += NavigationHelper_LoadState;
            _navigationHelper.SaveState += NavigationHelper_SaveState;
            _timer = new DispatcherTimer();
            _timer.Tick += (s, ev) => UpdateUI();
        }

        protected abstract void UpdateUI();

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        protected static async Task<bool> AskForReview()
        {
            bool askForReview = false;

            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (!settings.Values.Keys.Contains("askforreview"))
            {
                settings.Values.Add("askforreview", false);
            }
            else
            {
                settings.Values["askforreview"] = false;
            }

            settings.Values["askforreview"] = false;
            int started = 0;

            if (settings.Values.Keys.Contains("started"))
            {
                started = (int)settings.Values["started"];
                ++started;
            }
            else
            {
                settings.Values.Add("started", started);
            }

            if (started == 10)
            {
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["askforreview"] = true;
                askForReview = true;
            }
            if (askForReview)
            {
                var md = new Windows.UI.Popups.MessageDialog("Thank you for using StackCook.", "Please review my app");
                bool? reviewresult = null;
                md.Commands.Add(new Windows.UI.Popups.UICommand("OK", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = true)));
                md.Commands.Add(new Windows.UI.Popups.UICommand("Cancel", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = false)));
                await md.ShowAsync();
                if (reviewresult == true)
                {
                    return reviewresult.HasValue ? reviewresult.Value : false;
                }
            }
            return false;
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedFrom(e);
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            string param = e.NavigationParameter as string;
            if (!param.NullOrEmpty())
            {
                //SetTagOnTileVisibility(Visibility.Visible, param);
                Questions = new GeneratorIncrementalLoadingClass(this._maxPages, false, param);
            }
            else // First time.
            {
                //SetTagOnTileVisibility(Visibility.Collapsed);
                Questions = new GeneratorIncrementalLoadingClass(_maxPages);
            }
            SetDisplayItems(param);                                    
            Questions.CollectionChanged += Questions_CollectionChanged;
        }

        private void Questions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var incrementalLoader = sender as GeneratorIncrementalLoadingClass;
            if (incrementalLoader != null && incrementalLoader.DoneLoading)
            {
                SetScrollBar(_scrollPositions); // Just use this to determine the resettoprev setting.
            }
        }

        protected virtual void SetScrollBar(ScrollPositions position)
        {
            if (this.ScrollViewer != null)
            {
                if (position == ScrollPositions.ScrollToBegin)
                {
                    ScrollToBegin();
                }
                else if (position == ScrollPositions.ScrollToEnd)
                {
#if WINDOWS_PHONE_APP
                    _scrollViewerHeight = _scrollViewer.VerticalOffset;
#else
                    ScrollViewerHeight = _scrollViewer.HorizontalOffset;
#endif

                }
                else if (position == ScrollPositions.ScrollToPrevious)
                {
#if WINDOWS_PHONE_APP
                    _scrollViewer.ChangeView(null, _scrollViewerHeight, null);
#else
                    _scrollViewer.ChangeView(ScrollViewerHeight, null, null);
#endif
                    _scrollPositions = ScrollPositions.ScrollNormal; //
                }
                var scrollBars = _scrollViewer.GetDescendantsOfType<ScrollBar>();
                var scrollBar = scrollBars.FirstOrDefault((x) => x.Orientation == Orientation.Horizontal);
                scrollBar.Scroll += ScrollBar_Scroll;
            }
        }

        private void ScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollEventType == ScrollEventType.EndScroll)
            {
                var bar = sender as ScrollBar;
                if (bar == null) return;

                if (bar.Minimum == e.NewValue)
                {
                    Questions.RemoveRange();
                }
            }
        }

        protected void ScrollToBegin()
        {
            if (this.ScrollViewer != null)
            {
#if WINDOWS_PHONE_APP
                _scrollViewer.ChangeView(null, 0, null);
#else
                _scrollViewer.ChangeView(0, null, null);
#endif
            }
        }
    }
}
