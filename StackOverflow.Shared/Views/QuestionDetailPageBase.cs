﻿using StackOverflow.Common;
using StackOverflow.Data;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace StackOverflow.Views
{
    public abstract class QuestionDetailPageBase : Page
    {
        private NavigationHelper navigationHelper;
        private DispatcherTimer _timer = null;
        protected GroupedItemsPage root = GroupedItemsPage.current;

        private int _questionId;
        private ObservableCollection<QuestionInfo> _questionInfo = null;
        private ObservableCollection<Question> _questions = null;
        private ObservableCollection<Comment> _comments = null;
        private ObservableCollection<Answer> _answers = null;
        private DataTransferManager _dataTransferManager = null;

        protected abstract void SetDisplayItems();
        



        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ObservableCollection<Question> Questions
        {
            get
            {
                return _questions;
            }

            set
            {
                _questions = value;
            }
        }

        public ObservableCollection<Comment> Comments
        {
            get
            {
                return _comments;
            }

            set
            {
                _comments = value;
            }
        }

        public ObservableCollection<Answer> Answers
        {
            get
            {
                return _answers;
            }

            set
            {
                _answers = value;
            }
        }


        public GroupedItemsPage Root
        {
            get
            {
                return root;
            }
        }

        public int QuestionId
        {
            get
            {
                return _questionId;
            }

            set
            {
                _questionId = value;
            }
        }

        public DispatcherTimer Timer
        {
            get
            {
                return _timer;
            }

            set
            {
                _timer = value;
            }
        }

        public ObservableCollection<QuestionInfo> QuestionInfo
        {
            get
            {
                return _questionInfo;
            }

            set
            {
                _questionInfo = value;
            }
        }

        public QuestionDetailPageBase()
        {
            Questions = new ObservableCollection<Question>();
            Comments = new ObservableCollection<Comment>();
            Answers = new ObservableCollection<Answer>();
            Timer = new DispatcherTimer();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += NavigationHelper_LoadState;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            navigationHelper.OnNavigatedTo(e);
            this._dataTransferManager = DataTransferManager.GetForCurrentView();
            this._dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            navigationHelper.OnNavigatedFrom(e);
            this._dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            GetShareContent(args.Request);
        }

        protected bool GetShareContent(DataRequest request)
        {
            bool succeeded = false;
            Uri dataPackageUri = new Uri(_questions[0].link);
            DataPackage requestData = request.Data;
            requestData.Properties.Title = _questions[0].title;
            requestData.Properties.ContentSourceApplicationLink = dataPackageUri;
            requestData.SetWebLink(dataPackageUri);
            succeeded = true;
            return succeeded;
        }

        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            QuestionId = (int)e.NavigationParameter;
            var detailInfo = await StackOverflowSource.Instance.GetQuestionDetailsAsync(QuestionId, GroupedItemsPage.current.UserDetails.AccessToken);
            SetSources(detailInfo);
            Timer.Tick += (s, ev) => CheckForUpdates(s, ev, QuestionId);
        }

        protected async virtual void CheckForUpdates(object s, object ev, int questionId)
        {
            bool newUpdate = await StackOverflowSource.Instance.CheckForUpdate(questionId, _questions[0].last_activity_date, root.UserDetails.AccessToken);
            Questions[0].UpdateAvailable = newUpdate;
        }

        protected void SetSources(QuestionInfo detailInfo)
        {
            if (detailInfo.items.Count > 0)
            {
                if (_questions.Count == 0)
                {
                    _questions.Add(detailInfo.items[0]);
                }
                else
                {
                    _questions[0] = detailInfo.items[0];
                }

                _comments.AddRange(detailInfo.items[0].comments, clearCollection: true);
                _answers.AddRange(detailInfo.items[0].answers, clearCollection: true);
                //QuestionInfo = new ObservableCollection<QuestionInfo>();
                //QuestionInfo.Add(detailInfo);
                SetDisplayItems();
            }
        }

        protected async void UpdateUI(Object sender, object e, int question_id)
        {
            var info = await StackOverflowSource.Instance.LoadQuestionAsync(question_id, root.UserDetails.AccessToken);
            SetSources(info);
        }


        protected async Task<bool> UpVote()
        {
            bool upvoted = false;
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login(false);
            }
            else
            {
                if (!Questions[0].upvoted)
                {
                    var info = await StackOverflowSource.SetUpVote<QuestionInfo>(QuestionId, Root.UserDetails.AccessToken);
                    _questions[0].score = info.items[0].score;
                    _questions[0].upvoted = info.items[0].upvoted;
                    this.UpdateLayout();
                    upvoted = true; 
                }
            }
            return upvoted;
        }

        protected async Task<bool> DownVoted()
        {
            bool downVoted = false;
            if (GroupedItemsPage.current.UserDetails.AccessToken.NullOrEmpty())
            {
                GroupedItemsPage.Login(false);
            }
            else
            {
                if (!_questions[0].downvoted)
                {
                    var info = await StackOverflowSource.SetDownVote<QuestionInfo>(_questionId, root.UserDetails.AccessToken);
                    _questions[0].score = info.items[0].score;
                    _questions[0].downvoted = info.items[0].downvoted;
                    this.UpdateLayout();
                    downVoted = true;
                }
            }
            return downVoted;
        }


    }
}
