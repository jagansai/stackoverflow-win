﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
using System;
using Windows.UI.Xaml.Media.Imaging;
using System.Net;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace SOQuestionNotifier
{

    [Windows.Foundation.Metadata.WebHostHidden]
    abstract class BindableBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Multicast event for property change notifications.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        protected bool SetProperty<T>(Expression<Func<T>> propertyExpression)
        {
            this.OnPropertyChanged(ExtractPropertyName(propertyExpression));
            return true;
        }

        protected bool SetProperty(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
            return true;
        }


        private static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            var memberExp = propertyExpression.Body as MemberExpression;
            if (memberExp == null)
            {
                throw new ArgumentException("Expression must be a MemberExpression.", "propertyExpression");
            }
            return memberExp.Member.Name;
        }


        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }    


    //#region EqualityComparer
    //// From Jon Skeet's answer in StackOverflow.
    //// http://stackoverflow.com/questions/188120/can-i-specify-my-explicit-type-comparator-inline
    ///// <summary>
    ///// Non-generic class to produce instances of the generic class,
    ///// optionally using type inference.
    ///// </summary>
    //public static class ProjectionEqualityComparer
    //{
    //    /// <summary>
    //    /// Creates an instance of ProjectionEqualityComparer using the specified projection.
    //    /// </summary>
    //    /// <typeparam name="TSource">Type parameter for the elements to be compared</typeparam>
    //    /// <typeparam name="TKey">Type parameter for the keys to be compared,
    //    /// after being projected from the elements</typeparam>
    //    /// <param name="projection">Projection to use when determining the key of an element</param>
    //    /// <returns>A comparer which will compare elements by projecting 
    //    /// each element to its key, and comparing keys</returns>
    //    public static ProjectionEqualityComparer<TSource, TKey> Create<TSource, TKey>(Func<TSource, TKey> projection)
    //    {
    //        return new ProjectionEqualityComparer<TSource, TKey>(projection);
    //    }

    //    /// <summary>
    //    /// Creates an instance of ProjectionEqualityComparer using the specified projection.
    //    /// The ignored parameter is solely present to aid type inference.
    //    /// </summary>
    //    /// <typeparam name="TSource">Type parameter for the elements to be compared</typeparam>
    //    /// <typeparam name="TKey">Type parameter for the keys to be compared,
    //    /// after being projected from the elements</typeparam>
    //    /// <param name="ignored">Value is ignored - type may be used by type inference</param>
    //    /// <param name="projection">Projection to use when determining the key of an element</param>
    //    /// <returns>A comparer which will compare elements by projecting
    //    /// each element to its key, and comparing keys</returns>
    //    public static ProjectionEqualityComparer<TSource, TKey> Create<TSource, TKey>
    //        (TSource ignored,
    //         System.Func<TSource, TKey> projection)
    //    {
    //        return new ProjectionEqualityComparer<TSource, TKey>(projection);
    //    }

    //}

    ///// <summary>
    ///// Class generic in the source only to produce instances of the 
    ///// doubly generic class, optionally using type inference.
    ///// </summary>
    //public static class ProjectionEqualityComparer<TSource>
    //{
    //    /// <summary>
    //    /// Creates an instance of ProjectionEqualityComparer using the specified projection.
    //    /// </summary>
    //    /// <typeparam name="TKey">Type parameter for the keys to be compared,
    //    /// after being projected from the elements</typeparam>
    //    /// <param name="projection">Projection to use when determining the key of an element</param>
    //    /// <returns>A comparer which will compare elements by projecting each element to its key,
    //    /// and comparing keys</returns>        
    //    public static ProjectionEqualityComparer<TSource, TKey> Create<TKey>(Func<TSource, TKey> projection)
    //    {
    //        return new ProjectionEqualityComparer<TSource, TKey>(projection);
    //    }
    //}

    ///// <summary>
    ///// Comparer which projects each element of the comparison to a key, and then compares
    ///// those keys using the specified (or default) comparer for the key type.
    ///// </summary>
    ///// <typeparam name="TSource">Type of elements which this comparer 
    ///// will be asked to compare</typeparam>
    ///// <typeparam name="TKey">Type of the key projected
    ///// from the element</typeparam>
    //public sealed class ProjectionEqualityComparer<TSource, TKey> : IEqualityComparer<TSource>
    //{
    //    readonly System.Func<TSource, TKey> projection;
    //    readonly IEqualityComparer<TKey> comparer;

    //    /// <summary>
    //    /// Creates a new instance using the specified projection, which must not be null.
    //    /// The default comparer for the projected type is used.
    //    /// </summary>
    //    /// <param name="projection">Projection to use during comparisons</param>
    //    public ProjectionEqualityComparer(System.Func<TSource, TKey> projection)
    //        : this(projection, null)
    //    {
    //    }

    //    /// <summary>
    //    /// Creates a new instance using the specified projection, which must not be null.
    //    /// </summary>
    //    /// <param name="projection">Projection to use during comparisons</param>
    //    /// <param name="comparer">The comparer to use on the keys. May be null, in
    //    /// which case the default comparer will be used.</param>
    //    public ProjectionEqualityComparer(System.Func<TSource, TKey> projection, IEqualityComparer<TKey> comparer)
    //    {
    //        if (projection == null)
    //        {
    //            throw new System.ArgumentNullException("projection");
    //        }
    //        this.comparer = comparer ?? EqualityComparer<TKey>.Default;
    //        this.projection = projection;
    //    }

    //    /// <summary>
    //    /// Compares the two specified values for equality by applying the projection
    //    /// to each value and then using the equality comparer on the resulting keys. Null
    //    /// references are never passed to the projection.
    //    /// </summary>
    //    public bool Equals(TSource x, TSource y)
    //    {
    //        if (x == null && y == null)
    //        {
    //            return true;
    //        }
    //        if (x == null || y == null)
    //        {
    //            return false;
    //        }
    //        return comparer.Equals(projection(x), projection(y));
    //    }

    //    /// <summary>
    //    /// Produces a hash code for the given value by projecting it and
    //    /// then asking the equality comparer to find the hash code of
    //    /// the resulting key.
    //    /// </summary>
    //    public int GetHashCode(TSource obj)
    //    {
    //        if (obj == null)
    //        {
    //            throw new ArgumentNullException("obj");
    //        }
    //        return comparer.GetHashCode(projection(obj));
    //    }
    //}



    //#endregion


    class Site
    {
        public int launch_date { get; set; }
        public int open_beta_date { get; set; }
        public int closed_beta_date { get; set; }
        public string site_state { get; set; }
        public string icon_url { get; set; }
        public string audience { get; set; }
        public string site_url { get; set; }
        public string api_site_parameter { get; set; }
        public string logo_url { get; set; }
        public string name { get; set; }
        public string site_type { get; set; }
    }

    class Inbox
    {
        public Site site { get; set; }
        public bool is_unread { get; set; }
        public int creation_date { get; set; }
        public int question_id { get; set; }
        public int comment_id { get; set; }
        public int answer_id { get; set; }
        public string item_type { get; set; }
        public string link { get; set; }
        public string body { get; set; }
        public string title { get; set; }
    }

    class InboxInfo
    {
        public IList<Inbox> items { get; set; }
    }


    sealed class Tag 
    {
        public bool has_synonyms { get; set; }
        public bool is_moderator_only { get; set; }
        public bool is_required { get; set; }
        public int count { get; set; }
        public string name { get; set; }
    }

    sealed class TagInfo
    {
        private List<Tag> _items = new List<Tag>();
        public List<Tag> items { get; set; }
        public bool has_more { get; set; }
        public int quota_max { get; set; }
        public int quota_remaining { get; set; }

        public void AddRange(IEnumerable<string> tagStrings)
        {
            
        }

        internal void AddRange(IEnumerable<IList<string>> tagList)
        {
            foreach (var tags in tagList)
            {
                foreach(var tag in tags)
                {
                    _items.Add(new Tag() { name = tag });
                }
            }
        }
    }



    sealed class BadgeCounts
    {
        public int bronze { get; set; }
        public int silver { get; set; }
        public int gold { get; set; }
    }

    sealed class User
    {
        public BadgeCounts badge_counts { get; set; }

        private string _aboutMe { get; set; }
        public string about_me 
        { 
            get
            {
                return string.Format(@"<table><tr><td>location</td><td>{0}</td></tr> <tr><td>location</td><td>{1}</td></tr> 
                          <tr><td>member for</td><td>{2}</td></tr>
                          <tr><td>reputation</td><td>{3}</td></tr></table>{4}", display_name, location, Utils.GetCreationDt(Utils.ConvertFromUnix(creation_date)), reputation, _aboutMe);

                //return string.Format("location\t{0}<br/>member for\t{1}<br/>reputation\t{2}<br/><br/>{3}", location, Utils.GetCreationDt(Utils.ConvertFromUnix(creation_date)), reputation, _aboutMe);
            }
            set
            {
                _aboutMe = value;
            }
        }
        public int account_id { get; set; }
        public bool is_employee { get; set; }
        public int last_modified_date { get; set; }
        public int last_access_date { get; set; }
        public int reputation_change_year { get; set; }
        public int reputation_change_quarter { get; set; }
        public int reputation_change_month { get; set; }
        public int reputation_change_week { get; set; }
        public int reputation_change_day { get; set; }
        public int reputation { get; set; }
        public int creation_date { get; set; }
        public string user_type { get; set; }
        public int user_id { get; set; }
        public int accept_rate { get; set; }
        public string location { get; set; }
        public string website_url { get; set; }
        public string link { get; set; }
        public string display_name { get; set; }
        public string profile_image { get; set; }

        private BitmapImage _profileImage = null;

        public BitmapImage ProfileImage
        {
            get
            {
                if (_profileImage == null)
                {
                    _profileImage = new BitmapImage(new Uri(profile_image));
                }
                return _profileImage;
            }
        }
    }

    sealed class UserInfo
    {
        public IList<User> items { get; set; }
        public bool has_more { get; set; }
        public int quota_max { get; set; }
        public int quota_remaining { get; set; }
    }



    sealed class Info
    {
        public bool has_more { get; set; }
        public int quota_max { get; set; }
        public int quota_remaining { get; set; }
    }


    sealed class Owner
    {
        public int reputation { get; set; }
        public int user_id { get; set; }
        public string user_type { get; set; }
        public int accept_rate { get; set; }
        public string profile_image { get; set; }
        public string display_name { get; set; }
        public string link { get; set; }
        public Info info { get; set; }

        public override string ToString()
        {
            return string.Format("display_name:{0}", display_name);
        }

    }

    sealed class Answer : SOQuestionNotifier.BindableBase
    {

        public Answer()
        {
            this.PropertyChanged += Answer_PropertyChanged;
        }

        void Answer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // default.
        }

        
        public List<Comment> comments { get; set; }
        private int _commentCount = 0;
        public int comment_count
        {
            get
            {
                return _commentCount;
            }
            set
            {
                _commentCount = value;
                SetProperty(() => comment_count);
            }
        }

        public Owner owner { get; set; }
        public bool is_accepted { get; set; }


        private int _score;
        public int score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                SetProperty("score");
            }
        }
        public int last_activity_date { get; set; }
        public int last_edit_date { get; set; }
        public long creation_date { get; set; }
        public int answer_id { get; set; }
        public int question_id { get; set; }

        private string _body;
        public string body
        {
            get
            {
                return _body;
            }
            set
            {
                _body = value;
                SetProperty("body");
            }
        }


        public Info info { get; set; }

        private bool _upvoted;
        public bool upvoted 
        { 
            get
            {
                return _upvoted;
            }
            set
            {
                _upvoted = value;
                SetProperty("upvoted");
            }
        }

        private bool _downvoted;
        public bool downvoted 
        {
            get
            {
                return _downvoted;
            }
            set
            {
                _downvoted = value;
                SetProperty("downvoted");
            }
        }

        private string _title;
        public string title 
        { 
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                SetProperty("title");
            }
        }


        internal bool CheckForUpdate(int last_activity_date)
        {
            return this.last_activity_date > last_activity_date;
        }
    }

    sealed class AnswerInfo
    {
        public IList<Answer> items { get; set; }
        public Info info { get; set; }
    }



    sealed class ReplyToUser
    {
        public int reputation { get; set; }
        public int user_id { get; set; }
        public string user_type { get; set; }
        public int accept_rate { get; set; }
        public string profile_image { get; set; }
        public string display_name { get; set; }
        public string link { get; set; }
        public Info info { get; set; }
    }


    sealed class Comment : SOQuestionNotifier.BindableBase
    {
        public Comment()
        {
            this.PropertyChanged += Comment_PropertyChanged;
        }

        void Comment_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // default.
        }
        
        private Owner _owner;
        public Owner owner 
        {             
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
                SetProperty(() => owner);
            }
        }

        public bool edited { get; set; }
        public int score { get; set; }

        private int _creationDt;
        public int creation_date
        { 
            get
            {
                return _creationDt;
            }
            set
            {
                _creationDt = value;
                SetProperty(() => creation_date);
            }
        }

        public string CreationDt
        {
            get
            {
                return Utils.GetCreationDt(Utils.ConvertFromUnix(creation_date));
            }
        }


        public int post_id { get; set; }
        public int comment_id { get; set; }

        private string _body;
        public string body {
            get
            {
                return _body;
            }
            set
            {
                _body = value;
                SetProperty(() => body);
            }
        }
    }

    sealed class Question : SOQuestionNotifier.BindableBase
    {
        public Question()
        {
            answers = new List<Answer>();
            comments = new List<Comment>();
            
            this.PropertyChanged += Question_PropertyChanged;
        }

        private void Question_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

        public Question(Question q)
        {
            if (q.answers != null)
            {
                answers = new List<Answer>(q.answers);
            }

            if (q.comments != null)
            {
                comments = new List<Comment>(q.comments);
            }
            this.PropertyChanged += Question_PropertyChanged;
        }

        //void Question_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //}

        public override bool Equals(object obj)
        {
            Question q = obj as Question;
            if (q == null)
            {
                return false;
            }

            return this.Equals(q);
        }

        public bool Equals(Question question)
        {
            // do some basic checks like, 
            // 1) if there is a change in the number of comments
            // 2) if there is any edit to the question
            // 3) if any new answers are added

            return this.comment_count == question.comment_count &&
                   this.body == question.body &&
                   this.answer_count == question.answer_count &&
                   this.score == question.score &&
                   this.title == question.title;
        }

        public override int GetHashCode()
        {
            return this.question_id;
        }

        public bool is_accepted { get; set; }

        public List<Answer> answers { get; set; }
        //public IList<string> tags;

        //public IEnumerable<string> MinTags
        //{
        //    get
        //    {
        //        return tags.Take(System.Math.Min(3, tags.Count)).OrderBy( x => x.Length);
        //    }
        //}

        public bool Consumed { get; set; }


        public Owner owner { get; set; }
        public List<Comment> comments { get; set; }
        public bool is_answered { get; set; }
        public int view_count { get; set; }
        public int answer_count { get; set; }

        private int _score;
        public int score 
        { 
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                SetProperty("score");
            }
        }



        public int last_activity_date { get; set; }
        public long creation_date { get; set; }
        public int last_edit_date { get; set; }
        public int question_id { get; set; }
        public string link { get; set; }

        private bool _upvoted;
        public bool upvoted 
        {
            get
            {
                return _upvoted;
            }

            set
            {
                _upvoted = value;
                SetProperty("upvoted");
            }
        }

        private bool _downvoted;
        public bool downvoted 
        {
            get
            {
                return _downvoted;
            }
            set
            {
                _downvoted = value;
                SetProperty("downvoted");
            }
        }

        private string _title = string.Empty;
        public string title 
        {
            get
            {
                return WebUtility.HtmlDecode(_title);
            }
            set
            {
                _title = value;
                SetProperty(() => title);
            }
        }

        private string _body;
        public string body
        {
            get
            {
                return _body;
            }
            set
            {
                _body = value;
                SetProperty("body");
            }
        }
        
        public string answers_label
        {
            get
            {
                return string.Format("{0}", answer_count == 1 ? "answer" : "answers");
            }
        }

        private int _commentCount = 0;
        public int comment_count 
        {
            get
            {
                return _commentCount;
            }
            set
            {
                _commentCount = value;
                SetProperty(() => comment_count);
            }
        }

        //private string _commentCount = string.Empty;
        //public string comment_count
        //{
        //    get
        //    {
        //        return string.Format("{0} {1}", comments.Count(), comments.Count() == 1 ? "comment" : "comments");
        //    }
        //    set
        //    {
        //        _commentCount = value;
        //        SetProperty(() => comment_count);
        //    }

        //}

        public string CreationDt
        {
            get
            {
                return Utils.GetCreationDt(Utils.ConvertFromUnix(creation_date));
            }
        }


        public override string ToString()
        {

            //            DateTime creation_dt = Utils.ConvertFromUnix(creation_date);
            //            return string.Format(@"Owner:{0}\n,
            //                            creation_date:{1}\n,
            //                            title:{2}\n
            //                            question_id:{3}", 
            //                                            owner.ToString(), 
            //                                            Utils.Elapsed(creation_dt), 
            //                                            title,
            //                                            question_id).Replace("\\n", Environment.NewLine);

            return string.Empty;
        }


        internal bool CheckForUpdate(int last_activity_date)
        {
            return this.last_activity_date > last_activity_date; 
        }

    }

    sealed class CommentInfo : SOQuestionNotifier.BindableBase
    {
        public CommentInfo()
        {
            this.PropertyChanged += CommentInfo_PropertyChanged;
        }

        void CommentInfo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // default.
        }


        public List<Comment> items { get; set; }
        public Info info { get; set; }
    }

    sealed class QuestionInfo : SOQuestionNotifier.BindableBase
    {

        public QuestionInfo()
        {
            items = new List<Question>();

            this.PropertyChanged += QuestionInfo_PropertyChanged;
        }

        public void Clear()
        {
            if (items != null) items.Clear();
            _updateAvailable = false;
        }

        public QuestionInfo(QuestionInfo info)
        {
            Copy(info);
        }

        void QuestionInfo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // default.    
        }

        private bool _updateAvailable = false;
        public bool UpdateAvailable
        {
            get
            {
                return _updateAvailable;
            }
            set
            {
                _updateAvailable = value;
                SetProperty("UpdateAvailable");
            }
        }

        public List<Question> items { get; set; }
        public Info info { get; set; }



        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in items)
            {
                sb.Append(item.ToString());
            }

            return sb.ToString();
        }


        internal void Copy(QuestionInfo questionInfo)
        {

            if (items == null)
            {
                items = new List<Question>(questionInfo.items.Count);
            }
            items.Clear();
            items.AddRange(questionInfo.items);
            UpdateAvailable = questionInfo.UpdateAvailable;
            this.info = questionInfo.info;
        }
    }
}

