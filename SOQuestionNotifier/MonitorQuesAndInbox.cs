﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using System.Linq;

namespace SOQuestionNotifier 
{
    public sealed class MonitorQuesAndInbox : IBackgroundTask
    {
        BackgroundTaskDeferral _deferral = null;
        IBackgroundTaskInstance _taskInstance = null;
        volatile bool _cancelled = false;
        string _accessToken = string.Empty;

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            //System.Diagnostics.Debug.WriteLine("Background " + taskInstance.Task.Name + " Starting...");

            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);
            _deferral = taskInstance.GetDeferral();
            _taskInstance = taskInstance;
            GetQsAndInboxItems();
        }

        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            throw new NotImplementedException();
        }

        private async void GetQsAndInboxItems()
        {
            int count = 0;
            if (! _accessToken.NullOrEmpty())
            {
                // Then get the inbox items.
                var inboxInfo = await QueryStackOverflow.QueryInboxAsync(1, _accessToken);
                int index = 0;
                if (inboxInfo.items != null)
                {
                    count = inboxInfo.items.Count;
                    foreach (var q in inboxInfo.items)
                    {
                        SendTileTextNotification(q.body, ++index);
                    }
                    SendBadgeNotification(count);
                }
            }

            if (5 - count > 0)
            {
                var questionsInfo = await QueryStackOverflow.QueryTaggedQuestions(string.Empty, 1, 5 - count);
                if (questionsInfo.items != null)
                {
                    foreach (var q in questionsInfo.items)
                    {
                        SendTileTextNotification(q.title, q.creation_date);
                    }                    
                }
            }
        }

        public static void SendTileTextNotification(string title, long creation_date)
        {
            TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(true);
            SendTileTextNotificationWide(title, creation_date);
         //   SendTileTextNotificationMedium(title);
        }

        private static void SendTileTextNotificationWide(string title, long creation_date)
        {
            {
                var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150PeekImageAndText01);
                var tileAttributes = tileXml.GetElementsByTagName("text");
                tileAttributes[0].AppendChild(tileXml.CreateTextNode(title));
                var tileNotification = new TileNotification(tileXml) { Tag = creation_date.ToString() };
                TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
            }
            {
                var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150PeekImageAndText01);
                var tileAttributes = tileXml.GetElementsByTagName("text");
                tileAttributes[0].AppendChild(tileXml.CreateTextNode(title));
                var tileNotification = new TileNotification(tileXml) { Tag = creation_date.ToString() };
                TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
            }
            //{
            //    var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150BlockAndText01);
            //    var tileAttributes = tileXml.GetElementsByTagName("text");
            //    tileAttributes[0].AppendChild(tileXml.CreateTextNode(title));
            //    var tileNotification = new TileNotification(tileXml);
            //    TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
            //}
        }

        private static void SendTileTextNotificationMedium(string title)
        {
            var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150PeekImageAndText01);
            var tileAttributes = tileXml.GetElementsByTagName("text");
            tileAttributes[0].AppendChild(tileXml.CreateTextNode(title));
            var tileNotification = new TileNotification(tileXml);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
        }

        private static void UpdateTiles(XmlDocument tileXml,  string title)
        {
           
        }

        private void SendBadgeNotification(int count)
        {            
            if (count > 0)
            {
                XmlDocument badgeXml = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeGlyph);
                XmlElement badgeElement = (XmlElement)badgeXml.SelectSingleNode("/badge");
                badgeElement.SetAttribute("value", count.ToString());
                BadgeNotification badge = new BadgeNotification(badgeXml);
                BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badge);
            }
        }

    }
}
